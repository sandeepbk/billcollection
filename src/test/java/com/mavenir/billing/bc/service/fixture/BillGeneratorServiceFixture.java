package com.mavenir.billing.bc.service.fixture;

import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.generator.grpc.BillingCollectionRequest;
import com.mavenir.billing.generator.grpc.BillingCollectionResponse;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Slf4j
public class BillGeneratorServiceFixture {

    public static boolean onCompleted = false;

    public static BillingCollectionRequest billingCollectionRequest() {

        return BillingCollectionRequest.newBuilder()
                .setBillingAccId("10000014")
                .setDueAmount(1000.00)
                .setBillDate("2020-12-25")
                .setInvoiceNumber("Inv-01")
                .build();
    }

    public static BillingCollectionResponse billingCollectionResponse() {
        return BillingCollectionResponse.getDefaultInstance();
    }

    public static StreamObserver<BillingCollectionResponse> getStreamObserver() {

        return new StreamObserverImpl<BillingCollectionResponse>();
    }

    private static class StreamObserverImpl<T> implements StreamObserver<T> {

        @Override
        public void onNext(T t) {
            log.info("OnNext {}", t.toString());
        }

        @Override
        public void onError(Throwable throwable) {
            log.error("onError {}", throwable);
        }

        @Override
        public void onCompleted() {
            log.info("onCompleted");
            onCompleted = true;
        }
    }

    public static Billing getBilling() {
        List<DueBill> dueBills = new ArrayList<>();
        List<DueBill> historyBills = new ArrayList<>();

        dueBills.add(getDueBill());
        historyBills.add(getHistoryBill());

        return Billing.builder()
                .billingAccId("10000014")
                .dunningProfileId("p1")
                .accountState("ACTIVE")
                .dueBill(dueBills)
                .historyBill(historyBills)
                .build();

    }

    public static DueBill getHistoryBill() {

        return DueBill.builder()
                .invoiceId("1235")
                .billDate(getDate())
                .outstandingAmount(0.0)
                .dueAmount(1000.00)
                .billState("ACTIVE")
                .build();
    }

    public static DueBill getDueBill() {

        return DueBill.builder()
                .invoiceId("1234")
                .billDate(getDate())
                .outstandingAmount(1000.00)
                .dueAmount(1000.00)
                .billState("ACTIVE")
                .build();
    }

    public static Date getDate() {
        Date date1 = new Date();
        try {
            String date = "2020-12-25";
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (Exception exception) {
            log.error("Exception Occurs {}", exception);
        }
        return date1;
    }
}
