package com.mavenir.billing.bc.service.fixture;

import java.util.ArrayList;
import java.util.List;

import com.mavenir.billing.bc.config.BillAccountMgmtConfiguration;
import com.mavenir.billing.bc.constants.BillCollectionConstants;
import com.mavenir.billing.bc.model.AccountDTOResponse;
import com.mavenir.billing.bc.model.AccountEmailDTOResponse;
import com.mavenir.billing.bc.model.AccountLocaleDTOResponse;
import com.mavenir.billing.bc.model.AccountNameDTOResponse;


public class BillAccountMgmtServiceFixture {
    public static final String ACCOUNT_ID ="1234567890";
    public static final String COMPANY_NAME = "Rakuten";
    public static final String ACCOUNT_TYPE = "INDIVIDUAL";
    public static final String EMAIL_ADDRESS = "test@email.com";
    public static final String EMAIL_NOTES = "Sample notes";
    
    public static BillAccountMgmtConfiguration getBillAccountMgmtConfiguration() {
        BillAccountMgmtConfiguration billAccountMgmtConfiguration = new BillAccountMgmtConfiguration();
        billAccountMgmtConfiguration.setSslFlag(true);
        billAccountMgmtConfiguration.setConnectionAddress("https://localhost");
        billAccountMgmtConfiguration.setContextPath("/api/v1/accounts/{accountId}");
        billAccountMgmtConfiguration.setPort("8080");
        
        return billAccountMgmtConfiguration;
    }
    
    public static AccountDTOResponse getAccountDTOResponse() {
        AccountDTOResponse response = new AccountDTOResponse();
        
        List<AccountEmailDTOResponse> emailList = new ArrayList<AccountEmailDTOResponse>();
        AccountEmailDTOResponse email = new AccountEmailDTOResponse();
        email.setAddress(EMAIL_ADDRESS);
        email.setNote(EMAIL_NOTES);
        email.setType(BillCollectionConstants.PRIMARY_EMAIL_TYPE);
        email.setVerified(true);
        emailList.add(email);
        
        response.setId(ACCOUNT_ID);
        response.setCompanyName(COMPANY_NAME);
        response.setType(ACCOUNT_TYPE);
        response.setEmails(emailList);
        
        List<AccountLocaleDTOResponse> locales = new ArrayList<AccountLocaleDTOResponse>();
        AccountLocaleDTOResponse locale = new AccountLocaleDTOResponse();
        AccountNameDTOResponse name = new AccountNameDTOResponse();
        
        name.setFirstName("あいこ");
        name.setLastName("はるま");
        
        locale.setName(name);
        locale.setCompanyName("まさひろ");
        
        response.setLocales(locales);
        
        return response;
        
    }
}
