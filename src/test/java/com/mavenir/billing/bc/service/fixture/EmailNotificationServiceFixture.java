package com.mavenir.billing.bc.service.fixture;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.mavenir.billing.bc.config.EmailNotificationConfiguration;
import com.mavenir.billing.bc.model.EmailDTOResponse;
import com.mavenir.billing.bc.model.EmailDetailsDTO;
import com.mavenir.billing.bc.model.EmailWithTemplateDTO;

public class EmailNotificationServiceFixture {

    public static final String EMAIL_TEMPLATE ="INVOICE";
    
    public static EmailNotificationConfiguration getemailNotificationConfiguration() {
        EmailNotificationConfiguration emailNotificationConfiguration = new EmailNotificationConfiguration();
        emailNotificationConfiguration.setSslFlag(true);
        emailNotificationConfiguration.setConnectionAddress("https://localhost");
        emailNotificationConfiguration.setContextPath("/api/v1/emails/sendEmailUsingTemplate/{templateId}");
        emailNotificationConfiguration.setPort("8080");
        
        return emailNotificationConfiguration;
    }
    
    public static EmailWithTemplateDTO getEmailWithTemplateDTO() {
        EmailWithTemplateDTO request = new EmailWithTemplateDTO();
        
        EmailDetailsDTO email = new EmailDetailsDTO();
        email.setAccountId(String.valueOf(Math.random()).replace("0.", ""));

        List<String> bccList = new ArrayList<String>();
        bccList.add("bcc1@email.com");
        email.setBcc(bccList);

        List<String> ccList = new ArrayList<String>();
        bccList.add("cc1@email.com");
        email.setCc(ccList);

        List<String> toList = new ArrayList<String>();
        bccList.add("to1@email.com");
        email.setTo(toList);

        email.setCompanyName("Rakuten");
        email.setEntityId("111");
        email.setEntityType("INDIVIDUAL");
        email.setFrom("from@email.com");

        Map<String, String> templateParams = new HashMap<String, String>();
        templateParams.put("key1", "value1");

        email.setTemplateParams(templateParams);

        request.setEmail(email);
        return request;
    }
    
    public static EmailDTOResponse getEmailDTOResponse(EmailWithTemplateDTO emailWithTemplateDTO) {
        EmailDTOResponse response = new EmailDTOResponse();
        
        response.setId(emailWithTemplateDTO.getEmail().getAccountId());
        response.setTo(emailWithTemplateDTO.getEmail().getTo());
        response.setEntityType(emailWithTemplateDTO.getEmail().getEntityType());
        response.setEntityId(emailWithTemplateDTO.getEmail().getEntityId());
        response.setSentTime(new Date());
        
        return response;
        
    }
}
