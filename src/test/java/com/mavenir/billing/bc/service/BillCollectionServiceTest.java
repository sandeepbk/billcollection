package com.mavenir.billing.bc.service;

import com.mavenir.billing.bc.action.SendEmailActionFixture;
import com.mavenir.billing.bc.config.BillCollectionDunningConfiguration;
import com.mavenir.billing.bc.config.DunningConfiguration;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.billing.bc.service.fixture.BillCollectionServiceFixture;
import com.mavenir.billing.bc.service.fixture.ProfileFixture;
import com.mavenir.dep.common.profileexecutor.ProfileExecutorLibrary;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.any;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class BillCollectionServiceTest {

    @Mock
    private BillCollectionDunningConfiguration billCollectionDunningConfiguration;

    @Mock
    private ProfileExecutorLibrary<String, String, Billing, DueBill> profileExecutorLibrary;

    private final String DOCUMENT_FORMAT = "1001_bsCollections::";

    @Mock
    private BillingRepository billingRepository;

    @Mock
    private BillDunningService billDunningService;

    @Mock
    private DunningConfiguration dunningConfiguration;

    @Autowired
    private BillCollectionService billCollectionService;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        billCollectionService = new BillCollectionService(billCollectionDunningConfiguration,
                billingRepository, billDunningService, dunningConfiguration);
    }

    @DisplayName("Verify payment due date exceeded")
    @Test
    public void paymentDueDateExceeded_true_success() {
    try {

        Mockito.doReturn(DOCUMENT_FORMAT).when(dunningConfiguration).getDocumentID();

        Mockito.doReturn(SendEmailActionFixture.getEmailBillingInfoList())
                .when(billingRepository)
                .getEmailBillingInfo(anyString());

        Mockito.doReturn(profileExecutorLibrary)
                    .when(billCollectionDunningConfiguration).getProfileExecutorLibrary();

        Mockito.doReturn("DUNNING1")
                .when(profileExecutorLibrary).processEvent(anyString(), anyString(), anyString(), any(), any());

        Mockito.doReturn(BillCollectionServiceFixture.getBilling().get())
                .when(billDunningService).getBillingIdFromDB(BillCollectionServiceFixture
                .getCollectionDocument()
                .getBillingAccId());

        Mockito.doNothing()
                .when(billDunningService)
                .processDunningEvent(BillCollectionServiceFixture.getBilling().get(),
                        BillCollectionServiceFixture.getDueBill(), "EVENT1");

        boolean result = billCollectionService.paymentDueDateExceeded(BillCollectionServiceFixture.getCollectionDocument());

        log.info("Result of billCollectionService is {}", result);
        assertThat(result).isEqualTo(true);
    } catch (Exception exception) {
            log.error("Following Error Occurs {}", exception);
    } }
}