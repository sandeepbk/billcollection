package com.mavenir.billing.bc.service;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mavenir.billing.bc.config.EmailNotificationConfiguration;
import com.mavenir.billing.bc.model.EmailDTOResponse;
import com.mavenir.billing.bc.model.EmailWithTemplateDTO;
import com.mavenir.billing.bc.service.fixture.EmailNotificationServiceFixture;

@SpringBootTest
class EmailNotificationServiceTest {

    @Autowired
    private EmailNotificationService emailNotificationService;

    private MockRestServiceServer mockServer;
    private ObjectMapper mapper = new ObjectMapper();

    @DisplayName("Verify Send Email Notification API Call")
    @Test
    public void testSendNotificationEmail() throws JsonProcessingException, URISyntaxException, RestClientException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        EmailNotificationConfiguration emailNotificationConfiguration = EmailNotificationServiceFixture
                .getemailNotificationConfiguration();

        String url = emailNotificationConfiguration.getConnectionAddress() + ":" + emailNotificationConfiguration.getPort()
                + emailNotificationConfiguration.getContextPath();
        url = url.replace("{templateId}", EmailNotificationServiceFixture.EMAIL_TEMPLATE);

        EmailWithTemplateDTO request = EmailNotificationServiceFixture.getEmailWithTemplateDTO();

        emailNotificationService.createRequestFactory();
        mockServer = MockRestServiceServer.createServer(emailNotificationService.restTemplate);

        mockServer.expect(ExpectedCount.once(), requestTo(new URI(url))).andExpect(method(HttpMethod.POST))
                .andRespond(withStatus(HttpStatus.ACCEPTED).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(EmailNotificationServiceFixture.getEmailDTOResponse(request))));

        EmailDTOResponse response = emailNotificationService.sendNotificationEmail(request,
                EmailNotificationServiceFixture.EMAIL_TEMPLATE);
        mockServer.verify();

        assertTrue(response.getId().equals(request.getEmail().getAccountId()));
        assertTrue(response.getTo().equals(request.getEmail().getTo()));
        assertTrue(response.getEntityId().equals(request.getEmail().getEntityId()));
        assertTrue(response.getEntityType().equals(request.getEmail().getEntityType()));
        assertNotNull(response.getSentTime());
    }
}
