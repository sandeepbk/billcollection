package com.mavenir.billing.bc.service;

import com.mavenir.billing.bc.client.CollectionSchedulerClient;
import com.mavenir.billing.bc.config.BillCollectionDunningConfiguration;
import com.mavenir.billing.bc.config.DunningConfiguration;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.billing.bc.service.fixture.BillPaymentServiceFixture;
import com.mavenir.billing.bc.service.fixture.ProfileFixture;
import lombok.extern.slf4j.Slf4j;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class BillPaymentServiceTest {


    @Mock
    private CollectionSchedulerClient collectionSchedulerClient;

    @Mock
    private BillDunningService billDunningService;

    @Mock
    private BillingRepository billingRepository;

    @Mock
    private BillCollectionDunningConfiguration billCollectionDunningConfiguration;

    @Mock
    private DunningConfiguration dunningConfiguration;

    private BillPaymentService billPaymentService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);

        billPaymentService = new BillPaymentService(collectionSchedulerClient,
                billDunningService, billingRepository,
                billCollectionDunningConfiguration, dunningConfiguration);
    }

    @DisplayName("Verify Dunning Event Removal")
    @Test
    public void dunningEventRemoval_success() {

        try {
            Mockito.doReturn(ProfileFixture.initProfileExecutor())
                    .when(billCollectionDunningConfiguration).getProfileExecutorLibrary();

            Mockito.doReturn(BillPaymentServiceFixture.getBilling())
                    .when(billDunningService).getBillingIdFromDB(BillPaymentServiceFixture
                    .getPaymentProducedEvent()
                    .getBssBillingId());

            List<DueBill> dueBillList = new ArrayList<>();
            dueBillList.add(BillPaymentServiceFixture.getHistoryBill());

            Mockito.doReturn(BillPaymentServiceFixture.SUCCESS)
                    .when(collectionSchedulerClient)
                    .deleteAccFromDunning(BillPaymentServiceFixture.getBillingAfterPayment(),
                            dueBillList);

            Mockito.doReturn(ProfileFixture.getStateMap())
                    .when(billCollectionDunningConfiguration).getStateMap();

            billPaymentService.dunningEventRemoval(BillPaymentServiceFixture.getPaymentProducedEvent());

            String schedularRespponse = collectionSchedulerClient
                    .deleteAccFromDunning(BillPaymentServiceFixture.getBillingAfterPayment(), dueBillList);

            Assertions.assertThat(schedularRespponse).isEqualTo("SUCCESS");

        } catch (Exception exception) {
            log.error("Exception Occurs {}", exception);
        }

    }
    
    @DisplayName("If bill paid fully delete due bill")
    @Test
    public void deleteBillPaidFull_true_success() {
        billPaymentService = new BillPaymentService();
        boolean flag = billPaymentService.deleteBillPaidData(BillPaymentServiceFixture.getBilling(), 1000, 1);
        Assertions.assertThat(flag).isEqualTo(true);
    }

    @DisplayName("If bill paid partial due bill is not deleted")
    @Test
    public void deleteBillPaidPartial_false_success() {
        billPaymentService = new BillPaymentService();
        boolean flag = billPaymentService.deleteBillPaidData(BillPaymentServiceFixture.getBilling(), 100,1);
        Assertions.assertThat(flag).isEqualTo(false);
    }

}