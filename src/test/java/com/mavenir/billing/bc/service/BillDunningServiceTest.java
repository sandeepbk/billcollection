package com.mavenir.billing.bc.service;

import com.mavenir.billing.bc.client.CollectionSchedulerClient;
import com.mavenir.billing.bc.config.BillCollectionDunningConfiguration;
import com.mavenir.billing.bc.config.DunningConfiguration;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.billing.bc.service.fixture.BillDunningServiceFixture;
import com.mavenir.billing.bc.service.fixture.ProfileFixture;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.mockito.ArgumentMatchers.anyString;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class BillDunningServiceTest {

    @Mock
    private CollectionSchedulerClient collectionSchedulerClient;

    @Mock
    private BillingRepository billingRepository;

    @Mock
    private DunningConfiguration dunningConfiguration;

    @Mock
    private BillCollectionDunningConfiguration billCollectionDunningConfiguration;

    private BillDunningService billDunningService;


    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);

        billDunningService = new BillDunningService(dunningConfiguration, collectionSchedulerClient,
                billingRepository, billCollectionDunningConfiguration);
    }

    @DisplayName("Verify Process Dunning Event")
    @Test
    public void processDunningEvent_true_success() {
        try {
            Mockito.doReturn(ProfileFixture.initProfileExecutor())
                    .when(billCollectionDunningConfiguration).getProfileExecutorLibrary();

            Mockito.doReturn(BillDunningServiceFixture.getInvoiceList())
                    .when(collectionSchedulerClient).addAccFromDunning(anyString(),
                    anyString(),
                    anyString(),
                    anyString());

            Billing billing = BillDunningServiceFixture.getBilling().get();
            DueBill dueBill = BillDunningServiceFixture.getDueBill();

            billDunningService.processDunningEvent(billing,
                    dueBill,
                    "EVENT1");

            Assertions.assertNotNull(billing.getDueBill().get(0).getDueDate());

        } catch (Exception exception) {
            log.error("Exception Occurs {}", exception);
        }

    }
}
