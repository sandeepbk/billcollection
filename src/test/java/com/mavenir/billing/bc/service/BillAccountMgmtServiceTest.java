package com.mavenir.billing.bc.service;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.method;
import static org.springframework.test.web.client.match.MockRestRequestMatchers.requestTo;
import static org.springframework.test.web.client.response.MockRestResponseCreators.withStatus;

import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.test.web.client.ExpectedCount;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mavenir.billing.bc.config.BillAccountMgmtConfiguration;
import com.mavenir.billing.bc.model.EmailBillingInfo;
import com.mavenir.billing.bc.service.fixture.BillAccountMgmtServiceFixture;

@SpringBootTest
class BillAccountMgmtServiceTest {
    @Autowired
    BillAccountMgmtService billAccountMgmtService;

    private MockRestServiceServer mockServer;
    private ObjectMapper mapper = new ObjectMapper();

    @DisplayName("Verify Account Management API Call")
    @Test
    void testGetEmailBillingInfo() throws JsonProcessingException, URISyntaxException, RestClientException,
            KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        BillAccountMgmtConfiguration billAccountMgmtConfiguration = BillAccountMgmtServiceFixture
                .getBillAccountMgmtConfiguration();

        String url = billAccountMgmtConfiguration.getConnectionAddress() + ":" + billAccountMgmtConfiguration.getPort()
                + billAccountMgmtConfiguration.getContextPath();
        url = url.replace("{accountId}", BillAccountMgmtServiceFixture.ACCOUNT_ID);

        billAccountMgmtService.createRequestFactory();
        mockServer = MockRestServiceServer.createServer(billAccountMgmtService.restTemplate);

        mockServer.expect(ExpectedCount.once(), requestTo(new URI(url))).andExpect(method(HttpMethod.GET))
                .andRespond(withStatus(HttpStatus.OK).contentType(MediaType.APPLICATION_JSON)
                        .body(mapper.writeValueAsString(BillAccountMgmtServiceFixture.getAccountDTOResponse())));

        EmailBillingInfo response = billAccountMgmtService.getEmailBillingInfo(BillAccountMgmtServiceFixture.ACCOUNT_ID);
        mockServer.verify();

        assertTrue(response.getAccountId().equals(BillAccountMgmtServiceFixture.ACCOUNT_ID));
        assertTrue(response.getCompanyName().equals(BillAccountMgmtServiceFixture.COMPANY_NAME));
        assertTrue(response.getEntityId().equals(BillAccountMgmtServiceFixture.ACCOUNT_ID));
        assertTrue(response.getEntityType().equals(BillAccountMgmtServiceFixture.ACCOUNT_TYPE));
        assertTrue(response.getDestEmailAddress().equals(BillAccountMgmtServiceFixture.EMAIL_ADDRESS));
    }
}
