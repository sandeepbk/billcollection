package com.mavenir.billing.bc.service.fixture;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mavenir.billing.bc.action.*;
import com.mavenir.billing.bc.constants.BillCollectionConstants;
import com.mavenir.billing.bc.exception.ProcessException;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.dep.common.profileexecutor.Action;
import com.mavenir.dep.common.profileexecutor.ProfileExecutorInitializationException;
import com.mavenir.dep.common.profileexecutor.ProfileExecutorLibrary;
import com.mavenir.dep.common.profileexecutor.config.Profile;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;

@Slf4j
public class ProfileFixture {

    public static ProfileExecutorLibrary<String, String, Billing, DueBill> initProfileExecutor(){
        TypeReference<Map<String, Profile<String, String>>> profiles = new TypeReference<>() {};
        log.info("ProfileExecutorLibrary is invoked ");

        ProfileExecutorLibrary<String, String, Billing, DueBill> profileExecutorLibrary = null;
        try {
            boolean jsonFile = true; //getFileType();
            profileExecutorLibrary = new ProfileExecutorLibrary<>(getFilePath(), profiles, jsonFile);
            Map<String, Action<Billing, DueBill>> actionMap = new HashMap<>();
            actionMap.put(BillCollectionConstants.SEND_EMAIL, new SendEmailAction());
            actionMap.put(BillCollectionConstants.SCHEDULE, new ScheduleAction());
            actionMap.put(BillCollectionConstants.SUSPENDED, new SuspendedAction());
            actionMap.put(BillCollectionConstants.PENALTY, new PenaltyAction());
            actionMap.put(BillCollectionConstants.TERMINATE, new TerminateAction());
            profileExecutorLibrary.registerActions(actionMap);
        } catch (ProfileExecutorInitializationException e) {
            log.error("Exception from Profile execution library ", e);
            e.printStackTrace();
        }
        return profileExecutorLibrary;
    }

    private boolean getFileType() throws ProcessException {
        String profileFilePath = getFilePath(); //dunningConfiguration.getProfileFilePath();
        int index = profileFilePath.indexOf('.');
        String fileType = profileFilePath.substring(index+1,profileFilePath.length());
        if(BillCollectionConstants.JSON.equals(fileType)){
            return true;
        } else if(BillCollectionConstants.YAML.equals(fileType)){
            return true;
        } else {
            throw new ProcessException("File type is inappropriate");
        }
    }

    public static Map<String, Map<Integer, String>> getEventMap(){
        Map<String, Map<Integer, String>> eventMap = new HashMap<>();
        Map<Integer, String> subEventMap = new HashMap<>();
        subEventMap.put(1,"EVENT0");
        subEventMap.put(2,"EVENT1");
        subEventMap.put(3,"EVENT2");
        eventMap.put("p1", subEventMap);
        return eventMap;
    }

    public static Map<String, Map<Integer, String>> getStateMap(){
        Map<String, Map<Integer, String>> stateMap = new HashMap<>();
        Map<Integer, String> subStateMap = new HashMap<>();
        subStateMap.put(1,"INITIAL");
        subStateMap.put(2,"ACTIVE");
        subStateMap.put(3,"DUNNING1");
        stateMap.put("p1", subStateMap);
        return stateMap;
    }

    private static String getFilePath() {
        return "src/test/profiles.json";
    }
}
