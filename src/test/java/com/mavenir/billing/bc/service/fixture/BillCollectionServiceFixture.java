package com.mavenir.billing.bc.service.fixture;

import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.CollectionDocument;
import com.mavenir.billing.bc.model.DueBill;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class BillCollectionServiceFixture {

    public static CollectionDocument getCollectionDocument() {
        return CollectionDocument.builder()
                .billingAccId("bcs_10000014")
                .event("EVENT1")
                .invoiceId("1234")
                .build();
    }

    public static Optional<Billing> getBilling() {
        List<DueBill> dueBills = new ArrayList<>();
        List<DueBill> historyBills = new ArrayList<>();

        dueBills.add(getDueBill());
        historyBills.add(getHistoryBill());

        Billing billing = Billing.builder()
                .billingAccId("bcs_10000014")
                .dunningProfileId("p1")
                .accountState("ACTIVE")
                .dueBill(dueBills)
                .historyBill(historyBills)
                .build();

        return Optional.of(billing);
    }

    public static DueBill getHistoryBill() {

        Date date1 = new Date();
        try {
            String date = "2020-11-25";
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (Exception exception) {
            log.error("Exception Occurs {}", exception);
        }

        return DueBill.builder()
                .invoiceId("1235")
                .billDate(date1)
                .outstandingAmount(0.0)
                .dueAmount(1000.00)
                .billState("ACTIVE")
                .build();
    }

    public static DueBill getDueBill() {
        Date date1 = new Date();
        try {
            String date = "2020-12-25";
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (Exception exception) {
            log.error("Exception Occurs {}", exception);
        }
        return DueBill.builder()
                .invoiceId("1234")
                .billDate(date1)
                .outstandingAmount(1000.00)
                .dueAmount(1000.00)
                .billState("ACTIVE")
                .build();
    }
}
