package com.mavenir.billing.bc.service.fixture;

import com.mavenir.billing.bc.constants.InvoiceStatus;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.bc.model.InvoiceResponse;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class InvoiceServiceFixture {

    @Getter
    private static String accountId = "100013";
    @Getter
    private static String fromDate = "2020-07-25";
    @Getter
    private static String toDate = "2020-12-25";

    public static Optional<Billing> getBilling() {
        List<DueBill> dueBills = new ArrayList<>();
        List<DueBill> historyBills = new ArrayList<>();

        dueBills.add(getDueBill());
        historyBills.add(getHistoryBill());

        Billing billing = Billing.builder()
                .dueBill(dueBills)
                .historyBill(historyBills)
                .build();

        return Optional.of(billing);
    }

    public static List<InvoiceResponse> getInvoiceResponse() {

        List<InvoiceResponse> invoiceResponses = new ArrayList<>();
        invoiceResponses.clear();

        InvoiceResponse invoiceResponse1 = InvoiceResponse
                .builder()
                .invoiceNumber("Inv-1")
                .amountPaid(1000.00)
                .balance(0.0)
                .date("2020-11-25")
                .status(InvoiceStatus.Paid.name())
                .build();

        InvoiceResponse invoiceResponse2 = InvoiceResponse
                .builder()
                .invoiceNumber("Inv-2")
                .amountPaid(0.0)
                .balance(1000.00)
                .date("2020-12-25")
                .status(InvoiceStatus.Unpaid.name())
                .build();

        invoiceResponses.add(invoiceResponse1);
        invoiceResponses.add(invoiceResponse2);

        return invoiceResponses;
    }

    public static DueBill getHistoryBill() {

        Date date1 = new Date();
        try {
            String date = "2020-11-25";
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (Exception exception) {
            log.error("Exception Occurs {}", exception);
        }

        return DueBill.builder()
                .invoiceId("Inv-1")
                .billDate(date1)
                .outstandingAmount(0.0)
                .dueAmount(1000.00)
                .build();
    }

    public static DueBill getDueBill() {

        Date date1 = new Date();
        try {
            String date = "2020-12-25";
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (Exception exception) {
            log.error("Exception Occurs {}", exception);
        }
        return DueBill.builder()
                .invoiceId("Inv-2")
                .billDate(date1)
                .outstandingAmount(1000.00)
                .dueAmount(1000.00)
                .build();
    }

    public static ImmutablePair<Date, Date> getDates() {
        return ImmutablePair.of(new Date(), new Date());
    }
}
