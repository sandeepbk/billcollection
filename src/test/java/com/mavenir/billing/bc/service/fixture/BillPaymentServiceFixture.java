package com.mavenir.billing.bc.service.fixture;

import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.bc.model.PaymentProducedEvent;
import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
public class BillPaymentServiceFixture {

    public static final String SUCCESS ="SUCCESS";

    public static Billing getBilling(){
        List<DueBill> dueBillList = new ArrayList<>();
        List<DueBill> historyBillList = new ArrayList<>();
        dueBillList.add(getDueBill());

        Billing billing = Billing.builder()
                .billingAccId("10001")
                .accountState("ACTIVE")
                .suspenseBal(0.0)
                .dunningProfileId("p1")
                .dueBill(dueBillList)
                .historyBill(historyBillList)
                .build();

        return billing;
    }

    public static Billing getBillingAfterPayment(){
        List<DueBill> historyBillList = new ArrayList<>();
        List<DueBill> dueBillList = new ArrayList<>();
        historyBillList.add(getHistoryBill());

        Billing billing = Billing.builder()
                .billingAccId("10001")
                .accountState("ACTIVE")
                .suspenseBal(2000.0)
                .dunningProfileId("p1")
                .dueBill(dueBillList)
                .historyBill(historyBillList)
                .build();
        return billing;
    }

    public static DueBill getHistoryBill() {

        return DueBill.builder()
                .invoiceId("1234")
                .billDate(getDate())
                .dueAmount(1000.00)
                .outstandingAmount(0.0)
                .billState("ACTIVE")
                .billHashCode(1463532780)
                .build();
    }

    public static DueBill getDueBill() {

        return DueBill.builder()
                .invoiceId("1234")
                .billDate(getDate())
                .outstandingAmount(1000.00)
                .dueAmount(1000.00)
                .billState("ACTIVE")
                .build();
    }

    public static PaymentProducedEvent getPaymentProducedEvent() {

        return PaymentProducedEvent.builder()
                .bssBillingId("10001")
                .bkPaymentAmount(2500D)
                .build();
    }

    public static Date getDate() {
        Date date1 = new Date();
        try {
            String date = "2020-12-25";
            date1 = new SimpleDateFormat("yyyy-MM-dd").parse(date);
        } catch (Exception exception) {
            log.error("Exception Occurs {}", exception);
        }
        return date1;
    }
}
