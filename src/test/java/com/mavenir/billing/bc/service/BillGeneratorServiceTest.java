package com.mavenir.billing.bc.service;

import com.mavenir.billing.bc.config.BillCollectionDunningConfiguration;
import com.mavenir.billing.bc.config.DunningConfiguration;
import com.mavenir.billing.bc.exception.ProcessException;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.billing.bc.service.fixture.BillGeneratorServiceFixture;
import com.mavenir.billing.bc.service.fixture.ProfileFixture;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import static org.assertj.core.api.Assertions.assertThat;

public class BillGeneratorServiceTest {

    private String ktab = "1001_bsCollections";

    @Mock
    private BillingRepository billingRepository;

    @Mock
    private BillDunningService billDunningService;

    @Mock
    private DunningConfiguration dunningConfiguration;

    @Mock
    private BillCollectionDunningConfiguration billCollectionDunningConfiguration;

    private BillGeneratorService billGeneratorService;

    @BeforeEach
    public void init() {
        MockitoAnnotations.initMocks(this);
        billGeneratorService = new BillGeneratorService(ktab, billingRepository,
                billDunningService, dunningConfiguration, billCollectionDunningConfiguration);
    }

    @DisplayName("Verify Generate Bill")
    @Test
    public void generateBill_success() throws ProcessException {

        Mockito.doReturn(BillGeneratorServiceFixture.getBilling())
                .when(billDunningService).getBillingIdFromDB(BillGeneratorServiceFixture
                .billingCollectionRequest()
                .getBillingAccId());

        Mockito.doReturn("1001_bsCollections::")
                .when(dunningConfiguration).getDocumentID();

        Mockito.doReturn(ProfileFixture.initProfileExecutor())
                .when(billCollectionDunningConfiguration).getProfileExecutorLibrary();

        Mockito.doReturn(ProfileFixture.getEventMap())
                .when(billCollectionDunningConfiguration).getEventMap();

        Mockito.doReturn(ProfileFixture.getStateMap())
                .when(billCollectionDunningConfiguration).getStateMap();

        billGeneratorService.generateBill(BillGeneratorServiceFixture.billingCollectionRequest(),
                BillGeneratorServiceFixture.getStreamObserver());

        assertThat(BillGeneratorServiceFixture.onCompleted).isEqualTo(true);
    }
}
