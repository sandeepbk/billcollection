package com.mavenir.billing.bc.service;

import static org.assertj.core.api.Assertions.assertThat;

import static org.mockito.ArgumentMatchers.any;
import com.mavenir.billing.bc.config.DunningConfiguration;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.InvoiceResponse;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.billing.bc.service.fixture.InvoiceServiceFixture;
import com.mavenir.billing.bc.util.CollectionUtils;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class InvoiceServiceTest {

    @Autowired
    private InvoiceService invoiceService;

    @Mock
    private CollectionUtils collectionUtils;

    @Mock
    private BillingRepository billingRepository;

    @Mock
    private DunningConfiguration dunningConfiguration;

    private final String DOCUMENT_FORMAT = "1001_bsCollections::";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        invoiceService = new InvoiceService(billingRepository, dunningConfiguration, collectionUtils);
    }

    @DisplayName("Fetch Invoice Against Given AccountId")
    @Test
    public void getHistoryInvoicesByAccountId_success() {
        Optional<Billing> billing =  InvoiceServiceFixture.getBilling();
        try {
            Mockito.doReturn(DOCUMENT_FORMAT).when(dunningConfiguration).getDocumentID();

            Mockito.doReturn(billing)
                .when(billingRepository)
                    .findById(DOCUMENT_FORMAT.concat(InvoiceServiceFixture.getAccountId()));


            Mockito.doReturn(InvoiceServiceFixture.getDates())
                    .when(collectionUtils).getDates();

            Mockito.doReturn(Boolean.TRUE)
                    .when(collectionUtils).checkDateRange(any(), any(), any());

            Mockito.doReturn("2021-02-4")
                    .when(collectionUtils).dateToString(new Date());

            Mockito.doReturn(Double.valueOf("100.00"))
                    .when(collectionUtils)
                    .getPaidAmount(any(),any());

            List<InvoiceResponse> invoiceResponses = invoiceService
                    .getHistoryInvoicesByAccountId(InvoiceServiceFixture.getAccountId());

            log.info("invoices size {}", invoiceResponses.size());

            Assertions.assertNotNull(invoiceResponses);

        } catch (Exception exception) {
            log.error("Exception occurs {}", exception);
        }
    }

    @DisplayName("Fetch Invoice Against Given AccountId/Date Range")
    @Test
    public void getHistoryInvoicesByAccountIdAndDates_success() {

        Optional<Billing> billing =  InvoiceServiceFixture.getBilling();

        try {

            Mockito.doReturn(DOCUMENT_FORMAT).when(dunningConfiguration).getDocumentID();

            Mockito.doReturn(billing)
                    .when(billingRepository)
                    .findById(DOCUMENT_FORMAT.concat(InvoiceServiceFixture.getAccountId()));

            Mockito.doReturn(Boolean.TRUE)
                    .when(collectionUtils)
                    .validateDateRange(any(), any());

            Mockito.doReturn(new Date())
                    .when(collectionUtils)
                    .stringToDate(any());

            Mockito.doReturn(Boolean.TRUE)
                    .when(collectionUtils).checkDateRange(any(), any(), any());

            Mockito.doReturn("2021-02-4")
                    .when(collectionUtils).dateToString(any());

            Mockito.doReturn(Double.valueOf(100.00D))
                    .when(collectionUtils)
                    .getPaidAmount(any(),any());


            List<InvoiceResponse> invoiceResponses = invoiceService
                    .getHistoryInvoicesByDates(InvoiceServiceFixture.getAccountId(),
                            InvoiceServiceFixture.getFromDate(),
                            InvoiceServiceFixture.getToDate());

            Assertions.assertNotNull(invoiceResponses);

        } catch (Exception exception) {
            log.error("Exception occurs {}", exception);
        }
    }
}
