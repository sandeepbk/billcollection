package com.mavenir.billing.bc.util;

import com.mavenir.billing.bc.config.DunningConfiguration;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class CollectionUtilTest {

    @Mock
    private DunningConfiguration dunningConfiguration;

    private CollectionUtils collectionUtils;

    private String dateFormat = "yyyy-MM-dd";

    private String invoicesInDays = "365";

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        collectionUtils = new CollectionUtils(dunningConfiguration);
    }

    @DisplayName("Verify Invoice Status")
    @Test
   public void getInvoiceStatusForDueBill_Paid_Success() {

        if (Optional.ofNullable(collectionUtils).isEmpty()) {
            log.info("Null Value of collectionUtils");
        } else {
            log.info("CollectionUtils Value Found {}", collectionUtils);
        }

        String status = collectionUtils
                .getInvoiceStatusForDueBill(UtilFixture.getBalanceAmount(),UtilFixture.getDueAmount());

        String expectedStatus = UtilFixture.getInvoiceStatus_Paid();

        assertThat(status).isEqualTo(expectedStatus);
    }

    @DisplayName("Verify if BillDate is within provided date range")
    @Test
    public void checkDateRange_true_success() {

        Date billDate = UtilFixture.getBillDate();
        Date fromBillDate = UtilFixture.getFromBillDate();
        Date toBillDate = UtilFixture.getToBillDate();

        Mockito.doReturn(dateFormat)
                .when(dunningConfiguration)
                .getDateFormat();

        boolean result = collectionUtils.checkDateRange(fromBillDate, toBillDate, billDate);
        assertThat(result).isEqualTo(true);
    }

    @DisplayName("Validate the paid amount")
    @Test
    public void getPaidAmount_validated_success() {
        double balAmount = UtilFixture.getBalanceAmount();
        double dueAmount = UtilFixture.getDueAmount();
        double expPaidAmount = UtilFixture.getPaidAmount();

        double paidAmount = collectionUtils.getPaidAmount(dueAmount, balAmount);

        assertThat(paidAmount).isEqualTo(expPaidAmount);
    }

    @DisplayName("Validate Date Range is within 180 days")
    @Test
    public void validateDateRange_true_success() {
        final String fromDate = "2020-07-05";
        final String toDate = "2020-12-30";

        Mockito.doReturn(dateFormat)
                .when(dunningConfiguration)
                .getDateFormat();

        Mockito.doReturn(invoicesInDays)
                .when(dunningConfiguration)
                .getFetchInvoiceInDays();

        boolean actualResult = collectionUtils.validateDateRange(fromDate, toDate);

        assertThat(actualResult).isEqualTo(true);
    }

    @DisplayName("Validate Last Six Months Date Range")
    @Test
    public void validateDates() {

        Mockito.doReturn(invoicesInDays)
                .when(dunningConfiguration)
                .getFetchInvoiceInDays();

        ImmutablePair<Date, Date> dates =
                collectionUtils.getDates();

        Assertions.assertNotNull(dates);
    }
}
