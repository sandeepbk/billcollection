package com.mavenir.billing.bc.util;

import com.mavenir.billing.bc.constants.InvoiceStatus;

import lombok.Getter;

import java.util.Date;

public class UtilFixture {

    @Getter
    private static String invoiceStatus_Paid = InvoiceStatus.Paid.name();
    @Getter
    private static double balanceAmount = 0.0d;
    @Getter
    private static double dueAmount = 1000.00d;
    @Getter
    private static double paidAmount = 1000.00d;
    @Getter
    private static Date billDate = new Date(2020, 11, 25);
    @Getter
    private static Date fromBillDate = new Date(2020, 07, 01);
    @Getter
    private static Date toBillDate = new Date(2020, 12,31);

}
