package com.mavenir.billing.bc.action;

import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.EmailBillingInfo;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.billing.bc.service.BillAccountMgmtService;
import com.mavenir.billing.bc.service.EmailNotificationService;

import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.mockito.ArgumentMatchers.anyString;
import static org.assertj.core.api.Assertions.assertThat;

@Slf4j
@ExtendWith(MockitoExtension.class)
public class SendEmailActionTest {

    @Mock
    EmailNotificationService emailNotificationService;

    @Mock
    BillAccountMgmtService billAccountMgmtService;

    @Mock
    private BillingRepository billingRepository;

    @Autowired
    private SendEmailAction sendEmailAction;

    @Before
    public void init() {
        MockitoAnnotations.initMocks(this);
        sendEmailAction = new SendEmailAction(emailNotificationService, billAccountMgmtService, billingRepository);
    }

    @DisplayName("Fetch Email Billing info Against Given BillingAccountId")
    @Test
    public void getEmailBillingInfoByBillingAccId_success() throws Exception {
        Optional<Billing> billing = SendEmailActionFixture.getBilling();
        try {
            Mockito.doReturn(SendEmailActionFixture.getEmailBillingInfo()).when(billAccountMgmtService)
                    .getEmailBillingInfo(anyString());

            EmailBillingInfo emailBillingInfo = sendEmailAction.getEmailBillingInfoId(billing.get().getBillingAccId());

            assertThat(emailBillingInfo.getAccountId()).isEqualTo(SendEmailActionFixture.getEmailBillingInfo().getAccountId());

        } catch (Exception exception) {
            log.error("Exception occurs {}", exception);
            throw exception;
        }
    }
}