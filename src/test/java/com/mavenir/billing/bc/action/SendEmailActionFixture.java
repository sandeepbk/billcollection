package com.mavenir.billing.bc.action;


import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.bc.model.EmailBillingInfo;
import com.mavenir.billing.bc.service.fixture.BillCollectionServiceFixture;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class SendEmailActionFixture {

    public static EmailBillingInfo getEmailBillingInfo() {
        return EmailBillingInfo.builder()
                .accountId("10005")
                .destEmailAddress("info@rakuten.com")
                .entityId("bill_123")
                .entityType("BillingAccountPIC")
                .companyName("Rakuten Mobile")
                .build();
    }

    public static Optional<Billing> getBilling() {
        List<DueBill> dueBills = new ArrayList<>();
        List<DueBill> historyBills = new ArrayList<>();

        dueBills.add(BillCollectionServiceFixture.getDueBill());
        historyBills.add(BillCollectionServiceFixture.getHistoryBill());

        Billing billing = Billing.builder()
                .billingAccId("10005")
                .dunningProfileId("p1")
                .accountState("ACTIVE")
                .dueBill(dueBills)
                .historyBill(historyBills)
                .build();

        return Optional.of(billing);
    }

    public static List<EmailBillingInfo> getEmailBillingInfoList() {
        List<EmailBillingInfo> emailBillingInfos = new ArrayList<>();
        emailBillingInfos.add(getEmailBillingInfo());
        return emailBillingInfos;
    }
}
