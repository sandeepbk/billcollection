/*
 * Copyright (c) 2020. Mavenir Systems.
 */

package com.mavenir.billing.bc.config;

import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

@Configuration
public class BillCollectionCouchbaseConfiguration {

    @Autowired
    Cluster cluster;

    @Autowired
    Environment environment;

    @Bean
	public Bucket couchbaseBucket() {
		return cluster.bucket(environment
				.getRequiredProperty("spring.couchbase.bucket-name"));
	}
}
