package com.mavenir.billing.bc.config;

import com.fasterxml.jackson.core.type.TypeReference;
import com.mavenir.billing.bc.action.*;
import com.mavenir.billing.bc.constants.BillCollectionConstants;
import com.mavenir.billing.bc.exception.ProcessException;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.dep.common.profileexecutor.Action;
import com.mavenir.dep.common.profileexecutor.ProfileExecutorInitializationException;
import com.mavenir.dep.common.profileexecutor.ProfileExecutorLibrary;
import com.mavenir.dep.common.profileexecutor.config.Profile;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Component
@Getter
public class BillCollectionDunningConfiguration {

    @Autowired
    DunningConfiguration dunningConfiguration;

    @Autowired
    SendEmailAction sendEmailAction;

    @Autowired
    ScheduleAction scheduleAction;

    @Autowired
    SuspendedAction suspendedAction;

    @Autowired
    PenaltyAction penaltyAction;

    @Autowired
    TerminateAction terminateAction;

    @Autowired
    ResumeAction resumeAction;

    ProfileExecutorLibrary<String, String, Billing, DueBill> profileExecutorLibrary;

    Map<String, Map<Integer, String>> stateMap;

    Map<String, Map<Integer, String>> eventMap;

    /**
     * @return ProfileExecutorLibrary
     */
    @PostConstruct
    public ProfileExecutorLibrary<String, String, Billing, DueBill> init(){
        TypeReference<Map<String, Profile<String, String>>> profiles = new TypeReference<>() {};
        log.info("ProfileExecutorLibrary is invoked ");
        try {
            boolean jsonFile = getFileType();
            profileExecutorLibrary = new ProfileExecutorLibrary<>(dunningConfiguration.getProfileFilePath(), profiles, jsonFile);
            Map<String, Action<Billing, DueBill>> actionMap = new HashMap<>();
            actionMap.put(BillCollectionConstants.SEND_EMAIL, sendEmailAction);
            actionMap.put(BillCollectionConstants.SCHEDULE, scheduleAction);
            actionMap.put(BillCollectionConstants.SUSPENDED, suspendedAction);
            actionMap.put(BillCollectionConstants.PENALTY, penaltyAction);
            actionMap.put(BillCollectionConstants.TERMINATE, terminateAction);
            actionMap.put(BillCollectionConstants.RESUME, resumeAction);
            profileExecutorLibrary.registerActions(actionMap);
            stateMap = profileExecutorLibrary.getState();
            eventMap = profileExecutorLibrary.getEvent();
        } catch (ProfileExecutorInitializationException | ProcessException e) {
            log.error("Exception from Profile execution library ", e);
            e.printStackTrace();
        }
        return profileExecutorLibrary;
    }

    /**
     * @return boolean
     * @throws ProcessException
     */
    private boolean getFileType() throws ProcessException {
        String profileFilePath = dunningConfiguration.getProfileFilePath();
        int index = profileFilePath.indexOf('.');
        String fileType = profileFilePath.substring(index+1,profileFilePath.length());
        if(BillCollectionConstants.JSON.equals(fileType)){
          return true;
        } else if(BillCollectionConstants.YAML.equals(fileType)){
            return true;
        } else {
            throw new ProcessException("File type is inappropriate");
        }
    }
}
