package com.mavenir.billing.bc.config;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Data
@Getter
@AllArgsConstructor
@RequiredArgsConstructor
@Component
public class BillCollectionCsvConfiguration {

    String path;
    String penaltyPrefixName;
    String suspendedPrefixName;
    String terminatePrefixName;
    String activePrefixName;
    String extName;

    public BillCollectionCsvConfiguration(String path, String penaltyPrefixName, String suspendedPrefixName,
                                          String terminatePrefixName, String activePrefixName, String extName) {
        this.path = path;
        this.penaltyPrefixName = penaltyPrefixName;
        this.suspendedPrefixName = suspendedPrefixName;
        this.terminatePrefixName = terminatePrefixName;
        this.activePrefixName = activePrefixName;
        this.extName = extName;
    }

    @Autowired
    private CsvConfiguration csvConfiguration;

    @PostConstruct
    private void init() {
        new BillCollectionCsvConfiguration(csvConfiguration.getPath(), csvConfiguration.getPenaltyPrefixName(),
                csvConfiguration.getSuspendedPrefixName(), csvConfiguration.getTerminatePrefixName(),
                csvConfiguration.getActivePrefixName(), csvConfiguration.getExtName());
    }
}