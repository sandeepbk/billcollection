package com.mavenir.billing.bc.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Csv configuration file
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Configuration
@Component
@ConfigurationProperties(prefix = "csv")
public class CsvConfiguration {
    String path;
    String penaltyPrefixName;
    String suspendedPrefixName;
    String terminatePrefixName;
    String activePrefixName;
    String extName;
}
