package com.mavenir.billing.bc.config;

import com.mavenir.billing.account.grpc.BillCollectionAccountServiceGrpc;
import com.mavenir.billing.scheduler.grpc.BillCollectionSchedulerServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

@Component
@Data
public class BillCollectionGRpcConfiguration {

    private BillCollectionSchedulerServiceGrpc.BillCollectionSchedulerServiceBlockingStub billCollectionSchedulerServiceBlockingStub;

    private BillCollectionAccountServiceGrpc.BillCollectionAccountServiceBlockingStub billCollectionAccountServiceBlockingStub;

    @Autowired
    private GRpcConfiguration gRpcConfiguration;

    @PostConstruct
    private void init() {
        ManagedChannel schedulerManagedChannel = ManagedChannelBuilder.forAddress(gRpcConfiguration.getConnectionAddress(),
                gRpcConfiguration.getSchedulerPort()).usePlaintext().build();
        billCollectionSchedulerServiceBlockingStub = BillCollectionSchedulerServiceGrpc.newBlockingStub(schedulerManagedChannel);
        ManagedChannel accountManagedChannel = ManagedChannelBuilder.forAddress(gRpcConfiguration.getConnectionAddress(),
                gRpcConfiguration.getAccountPort()).usePlaintext().build();
        billCollectionAccountServiceBlockingStub = BillCollectionAccountServiceGrpc.newBlockingStub(accountManagedChannel);
    }
}
