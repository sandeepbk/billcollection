package com.mavenir.billing.bc.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Kafka configuration file
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Configuration
@ConfigurationProperties(prefix = "bc.kafka")
public class KafkaConfiguration {
	private String connectionAddress;
	private String groupId;
}
