package com.mavenir.billing.bc.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Couchbase configuration file
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Configuration
@ConfigurationProperties(prefix = "bc.couchbase")
public class CouchbaseConfiguration {
    private String connectionAddress;
    private String userName;
    private String password;
    private String billingBucket;
}
