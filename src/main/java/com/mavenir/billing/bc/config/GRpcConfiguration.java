package com.mavenir.billing.bc.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * GRpc configuration file
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Configuration
@ConfigurationProperties(prefix = "bc.grpc")
public class GRpcConfiguration {
    private String connectionAddress;
    private int schedulerPort;
    private int accountPort;
}
