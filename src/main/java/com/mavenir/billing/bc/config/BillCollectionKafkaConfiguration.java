package com.mavenir.billing.bc.config;

import com.mavenir.billing.bc.model.PaymentProducedEvent;
import com.mavenir.billing.bc.model.CollectionDocument;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.config.ConcurrentKafkaListenerContainerFactory;
import org.springframework.kafka.core.ConsumerFactory;
import org.springframework.kafka.core.DefaultKafkaConsumerFactory;
import org.springframework.kafka.support.serializer.JsonDeserializer;

import java.util.HashMap;
import java.util.Map;

@EnableKafka
@Configuration
public class BillCollectionKafkaConfiguration {

    @Autowired
    KafkaConfiguration kafkaConfiguration;

    @Bean
    public ConsumerFactory<String, CollectionDocument> dunningConsumerFactory() {
        JsonDeserializer<CollectionDocument> deserializer = new JsonDeserializer<>(CollectionDocument.class);
        deserializer.setRemoveTypeHeaders(false);
        deserializer.addTrustedPackages("*");
        deserializer.setUseTypeMapperForKey(true);
        Map<String, Object> config = new HashMap<>();
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfiguration.getConnectionAddress());
        config.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaConfiguration.getGroupId());
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, deserializer);
        return new DefaultKafkaConsumerFactory<String, CollectionDocument>(config, new StringDeserializer(),
                deserializer);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, CollectionDocument> kafkaListenerDunningContainerFactory() {
        ConcurrentKafkaListenerContainerFactory factory = new ConcurrentKafkaListenerContainerFactory();
        factory.setConsumerFactory(dunningConsumerFactory());
        return factory;
    }

    @Bean
    public ConsumerFactory<String, PaymentProducedEvent> paymentConsumerFactory() {
        JsonDeserializer<PaymentProducedEvent> deserializer = new JsonDeserializer<>(PaymentProducedEvent.class);
        deserializer.setRemoveTypeHeaders(false);
        deserializer.addTrustedPackages("*");
        deserializer.setUseTypeMapperForKey(true);
        Map<String, Object> config = new HashMap<>();
        config.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaConfiguration.getConnectionAddress());
        config.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaConfiguration.getGroupId());
        config.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
        config.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, deserializer);
        return new DefaultKafkaConsumerFactory<String, PaymentProducedEvent>(config, new StringDeserializer(),
                deserializer);
    }

    @Bean
    public ConcurrentKafkaListenerContainerFactory<String, PaymentProducedEvent> kafkaListenerPaymentContainerFactory() {
        ConcurrentKafkaListenerContainerFactory factory = new ConcurrentKafkaListenerContainerFactory();
        factory.setConsumerFactory(paymentConsumerFactory());
        return factory;
    }
}