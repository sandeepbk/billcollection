package com.mavenir.billing.bc.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * Dunning configuration file
 */
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Configuration
@ConfigurationProperties(prefix = "bc")
public class DunningConfiguration {
    private String profileFilePath;
    private String documentID;
    private String fetchInvoiceInDays;
    private String dateFormat;
}
