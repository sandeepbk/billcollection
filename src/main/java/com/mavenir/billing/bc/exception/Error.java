package com.mavenir.billing.bc.exception;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;

import java.util.Arrays;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum Error {
	// Invoice Related

	DOCUMENT_NOT_FOUND(101, "No Document Found for given accountId", 404),
	FETCH_HISTORY_INVOICE_FAILED(102, "Fetch History Invoice Failed", 500),
	FETCH_DUE_INVOICE_FAILED(103, "Fetch Due Invoice Failed", 500),
	DATE_RANGE_INVALID(104, "Invalid date range.Allowed range is 180 days only", 412),
	INVALID_REQUEST(105, "Invalid Request", 400);

	private final int code;
	private final String description;
	private final int httpStatusCode;

	Error(int code, String description, int httpStatusCode) {
		this.code = code;
		this.description = description;
		this.httpStatusCode = httpStatusCode;
	}

	public int getCode() {
		return code;
	}

	public String getDescription() {
		return description;
	}

	public HttpStatus getHttpStatus() {
        return HttpStatus.valueOf(httpStatusCode);
    }

	@Override
	public String toString() {
		return code + ": " + description;
	}
	
	@JsonCreator
	static Error findValue(@JsonProperty("code") int code, @JsonProperty("description") String description) {
		return Arrays.stream(values())
					 .filter(error -> error.code == code && error.description.equals(description))
					 .findFirst()
					 .get();
	}
}
