/**
 * Copyright (C) 2019 Mavenir Systems, Inc.
 *
**/
package com.mavenir.billing.bc.exception;

import lombok.EqualsAndHashCode;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

@EqualsAndHashCode(callSuper = false)
public class ServiceException extends ResponseStatusException {
    private final String operation;
	private final int errorCode;

	public ServiceException(String operation, int errorCode, String message, HttpStatus status) {
		super(status, message);
		this.operation = operation;
		this.errorCode = errorCode;
	}

	public ServiceException(String operation, Error error) {
		this(operation, error.getCode(), error.getDescription(), error.getHttpStatus());
	}

	public ServiceException(String operation, String failureMessage, Error error) {
		this(operation, error.getCode(), error.getDescription() + ":" + failureMessage, error.getHttpStatus());
	}

	public String getOperation() {
		return operation;
	}

	public int getErrorCode() {
		return errorCode;
	}
}
