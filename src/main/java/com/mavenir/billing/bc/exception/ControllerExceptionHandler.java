/**
* Copyright (C) 2019 Mavenir Systems, Inc.
 *
**/
package com.mavenir.billing.bc.exception;

import com.fasterxml.jackson.databind.JsonMappingException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Adds additional fields in response whenever customer management operations fail.
 * @author sjain
 *
 */
@ControllerAdvice
@RestController
@Slf4j
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {
    /**
     * Overriding base method to return controller exception body
     */
	@Override
	protected ResponseEntity<Object> handleExceptionInternal(
            Exception ex, @Nullable Object body, HttpHeaders headers, HttpStatus status, WebRequest request) {
		log.error("Server error", ex);

		String message = ex.getMessage();

		// For JSON errors identify the field to display cleaner message
		Throwable cause = ex.getCause();
		if (cause instanceof JsonMappingException) {
		    JsonMappingException jme = (JsonMappingException) cause;
		    String fieldName = "unknown";
		    try {
		       JsonMappingException.Reference ref = jme.getPath().get(0);
               fieldName = ref.getFieldName();
            } catch (Exception jex) {
		        logger.debug("Unable to extract JSON field name: ", jex);
            }
		    message = "Invalid value for the field '" + fieldName + "'";
        }

        ControllerExceptionResponse exceptionResponse = new ControllerExceptionResponse(status, message);

        return super.handleExceptionInternal(ex, exceptionResponse, headers, status, request);
	}

	/**
	 * The catch all exception handler for exceptions not covered by base class
	 */
	@ExceptionHandler(Exception.class)
	public final ResponseEntity<Object> handleAllExceptions(Exception ex, WebRequest request) {
		HttpHeaders headers = new HttpHeaders();
		HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;

		return this.handleExceptionInternal(ex, null, headers, status, request);
	}

	/**
	 * Service exception handler
	 */
	@ExceptionHandler(ServiceException.class)
	public final ResponseEntity<Object> handleServiceException(ServiceException ex, WebRequest request) {
		log.error("REST Service exception", ex);
		HttpHeaders headers = new HttpHeaders();
		headers.add("content-type","application/json");
		ControllerExceptionResponse exceptionResponse = new ControllerExceptionResponse(ex);

		return new ResponseEntity<Object>(exceptionResponse,headers,ex.getStatus());
	}
}
