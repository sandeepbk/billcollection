/**
 * Copyright (C) 2019 Mavenir Systems, Inc.
 *
**/
package com.mavenir.billing.bc.exception;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import org.springframework.http.HttpStatus;

import java.time.ZonedDateTime;

/**
 * The error response to be sent for an exception
 * @author sjain
 *
 */
@ApiModel(value = "error", description = "Entity representing a error response")
@Value
@Builder
@AllArgsConstructor
@JsonDeserialize(builder = ControllerExceptionResponse.ControllerExceptionResponseBuilder.class)
public class ControllerExceptionResponse {
    private final static String UNKNOWN_OPERATION = null;
    private final static int UNKNOWN_CODE = -1;

    @ApiModelProperty(notes = "Timestamp", example = "1579294361")
    private ZonedDateTime timestamp;
    @ApiModelProperty(notes = "HTTP Status", example = "404")
	private int status;
    @ApiModelProperty(notes = "HTTP Status message", example = "Not Found")
	private String statusMessage;
    @ApiModelProperty(notes = "Error description", example = "Entity does not exist")
	private String message;
    @ApiModelProperty(notes = "Error code", example = "401")
	private int code;
    @ApiModelProperty(notes = "Service operation", example = "get-customer-info")
	private String operation;

	@JsonPOJOBuilder(withPrefix = "")
    public static class ControllerExceptionResponseBuilder {
        // Lombok will add constructor, setters, build method
    }

    public ControllerExceptionResponse(HttpStatus httpStatus, String message, int code, String operation) {
	    this.timestamp = ZonedDateTime.now();
	    status = httpStatus.value();
        statusMessage = httpStatus.getReasonPhrase();
        this.message = message;
        this.code = code;
        this.operation = operation;
    }

	public ControllerExceptionResponse(HttpStatus httpStatus, String message) {
	    this(httpStatus, message, UNKNOWN_CODE, UNKNOWN_OPERATION);
    }

    public ControllerExceptionResponse(ServiceException ex) {
	    this(ex.getStatus(), ex.getReason(), ex.getErrorCode(), ex.getOperation());
    }
}
