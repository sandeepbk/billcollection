package com.mavenir.billing.bc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BillCollectionApplication {
	public static void main(String[] args) {
		SpringApplication.run(BillCollectionApplication.class, args);
	}
}
