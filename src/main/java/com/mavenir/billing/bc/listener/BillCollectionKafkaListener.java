package com.mavenir.billing.bc.listener;

import com.mavenir.billing.bc.exception.ProcessException;
import com.mavenir.billing.bc.model.PaymentProducedEvent;
import com.mavenir.billing.bc.model.CollectionDocument;
import com.mavenir.billing.bc.service.BillCollectionService;
import com.mavenir.billing.bc.service.BillPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class BillCollectionKafkaListener {

    @Autowired
    BillCollectionService billCollectionService;

    @Autowired
    BillPaymentService billPaymentService;

    @KafkaListener(topics = "${bc.kafka.topic.scheduler}", groupId = "group_billing_collection",
                    containerFactory = "kafkaListenerDunningContainerFactory")
    public void consume(@Payload CollectionDocument collectionDocument) throws ProcessException {
        try {
            log.info("Kafka message received for Dunning {}", collectionDocument);
            billCollectionService.paymentDueDateExceeded(collectionDocument);
        } catch (Exception e) {
            log.error("Dunning Kafka listener failed ",e);
            throw new ProcessException("Dunning Kafka listener failed",e);
        }
    }

    @KafkaListener(topics = "${bc.kafka.topic.payment-gateway}", groupId = "group_billing_collection",
            containerFactory = "kafkaListenerPaymentContainerFactory")
    public void payment(@Payload PaymentProducedEvent paymentProducedEvent) throws ProcessException {
        try {
            log.info("Kafka message received for Payment {}", paymentProducedEvent);
            billPaymentService.dunningEventRemoval(paymentProducedEvent);
        } catch (Exception e) {
            log.error("Payment Kafka listener failed ",e);
            throw new ProcessException("Payment Kafka listener failed",e);
        }
    }
}