package com.mavenir.billing.bc.client;

import com.mavenir.billing.bc.config.BillCollectionGRpcConfiguration;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.scheduler.grpc.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
@AllArgsConstructor
public class CollectionSchedulerClient {

    @Autowired
    private final BillCollectionGRpcConfiguration billCollectionGRPCConfiguration;

    /**
     * @param billingActId  "Billing Account ID"
     * @param invoiceId     "Invoice id for Billing Account ID"
     * @param event         "Event"
     * @param dunningBillDueDate    "Next Dunning bill due date "
     * @return  List    "List of Invoice data for Billing account id"
     */
    public List<Invoice> addAccFromDunning(String billingActId, String invoiceId, String event, String dunningBillDueDate) {
        AddAccFromDunningRequest.DueBills dueBills = AddAccFromDunningRequest.DueBills.newBuilder().
                setDunningBillDueDate(dunningBillDueDate).
                setEvent(event).
                setInvoiceId(invoiceId).build();
        AddAccFromDunningRequest addAccFromDunningRequest = AddAccFromDunningRequest.newBuilder().
                setBillingAccId(billingActId).
                addDueBills(dueBills).build();
        log.info("Next dunning invocation {}", addAccFromDunningRequest);
        BillCollectionSchedulerServiceGrpc.BillCollectionSchedulerServiceBlockingStub billCollectionSchedulerServiceBlockingStub =
                billCollectionGRPCConfiguration.getBillCollectionSchedulerServiceBlockingStub();
        AddAccFromDunningResponse addAccFromDunningResponse =
                billCollectionSchedulerServiceBlockingStub.addAccFromDunning(addAccFromDunningRequest);
        log.info("dunning reference id {}", addAccFromDunningResponse.getInvoice(0).getReferenceId());
        return addAccFromDunningResponse.getInvoiceList();
    }

    /**
     * @param billing
     * @param dueBillList
     * @return response
     */
    public String deleteAccFromDunning(Billing billing, List<DueBill> dueBillList) {
        String response = null;
        if (!dueBillList.isEmpty()) {
            DeleteAccFromDunningRequest deleteAccFromDunningRequest = null;
            Invoice invoice;
            for (DueBill dueBill : dueBillList) {
                invoice = Invoice.newBuilder().
                        setInvoiceId(dueBill.getInvoiceId()).
                        setReferenceId(dueBill.getSchedulerReferenceId()).build();
                deleteAccFromDunningRequest = DeleteAccFromDunningRequest.newBuilder().
                        setBillingAccId(billing.getBillingAccId()).
                        addInvoice(invoice).build();
            }
            log.info("Delete account for dunning request {}", deleteAccFromDunningRequest);
            BillCollectionSchedulerServiceGrpc.BillCollectionSchedulerServiceBlockingStub billCollectionSchedulerServiceBlockingStub =
                    billCollectionGRPCConfiguration.getBillCollectionSchedulerServiceBlockingStub();
            DeleteAccFromDunningResponse deleteAccFromDunningResponse = billCollectionSchedulerServiceBlockingStub.
                    deleteAccFromDunning(deleteAccFromDunningRequest);
            log.info("Delete billing account from dunning invoked {}", deleteAccFromDunningResponse.getResponseStatus());
            response = deleteAccFromDunningResponse.getResponseStatus();
        }
        return response;
    }
}