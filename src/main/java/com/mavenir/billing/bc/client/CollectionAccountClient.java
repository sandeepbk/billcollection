package com.mavenir.billing.bc.client;

import com.mavenir.billing.account.grpc.BillCollectionAccountServiceGrpc;
import com.mavenir.billing.account.grpc.BillPenaltyRequest;
import com.mavenir.billing.account.grpc.BillPenaltyResponse;
import com.mavenir.billing.bc.config.BillCollectionGRpcConfiguration;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
@AllArgsConstructor
public class CollectionAccountClient {

    @Autowired
    private final BillCollectionGRpcConfiguration billCollectionGRPCConfiguration;

    /**
     * @param billingActId      Account ID for billing
     * @param penaltyCharges    Penalty charges applied for account status moved to Suspended
     * @return  status          Response status code
     */
    public int addPenalty(String billingActId, double penaltyCharges) {
        BillPenaltyRequest billPenaltyRequest = BillPenaltyRequest.newBuilder().
                setBillingAccId(billingActId).
                setPenaltyCharges(penaltyCharges).build();
        log.info("Bill penalty request {}", billPenaltyRequest);
        BillCollectionAccountServiceGrpc.BillCollectionAccountServiceBlockingStub billCollectionAccountServiceBlockingStub =
                billCollectionGRPCConfiguration.getBillCollectionAccountServiceBlockingStub();
        BillPenaltyResponse billPenaltyResponse =
                billCollectionAccountServiceBlockingStub.addPenalty(billPenaltyRequest);
        log.info("Bill penalty status {}", billPenaltyResponse.getStatusCode());
        return billPenaltyResponse.getStatusCode();
    }
}
