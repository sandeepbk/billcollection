package com.mavenir.billing.bc.util;

import com.mavenir.billing.bc.config.DunningConfiguration;
import com.mavenir.billing.bc.constants.InvoiceStatus;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.Date;

@Slf4j
@NoArgsConstructor
@AllArgsConstructor
@Component
public class CollectionUtils {

    @Autowired
    private DunningConfiguration dunningConfiguration;

    public String getInvoiceStatusForDueBill(double balAmount, double dueAmount) {
        if(balAmount == 0.0d) {
            return InvoiceStatus.Paid.name();
        } else if (balAmount == dueAmount && dueAmount != 0.0d) {
            return InvoiceStatus.Unpaid.name();
        } else if(dueAmount != 0.0d && balAmount > 0 && balAmount < dueAmount) {
            return InvoiceStatus.PartiallyPaid.name();
        } else {
            return InvoiceStatus.Pending.name();
        }
    }

    public boolean checkDateRange(Date fromBillDate, Date toBillDate, Date actualBillDate) {

        SimpleDateFormat formatter = new SimpleDateFormat(dunningConfiguration.getDateFormat());

        String strFromBillDate = formatter.format(fromBillDate);
        String strToBillDate = formatter.format(toBillDate);
        String strActualBillDate = formatter.format(actualBillDate);

        log.info("strFromBillDate {}", strFromBillDate);
        log.info("strToBillDate {} ", strToBillDate);
        log.info("strActualBillDate {}", strActualBillDate);

        return ((actualBillDate.after(fromBillDate) && actualBillDate.before(toBillDate))
                || (Integer.valueOf(strFromBillDate).equals(Integer.valueOf(strActualBillDate)))
                || (Integer.valueOf(strToBillDate).equals(Integer.valueOf(strActualBillDate))));
    }

    public Double getPaidAmount(Double dueAmount, Double balAmount) {
        return dueAmount - balAmount;
    }

    public Date stringToDate(String date) {
        Date date1 = new Date();
        try {
            date1 = new SimpleDateFormat(dunningConfiguration.getDateFormat()).parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date1;
    }

    public String dateToString(Date date) {
        return new SimpleDateFormat(dunningConfiguration.getDateFormat()).format(date);

    }
    public boolean validateDateRange(String fromBillDate, String toBillDate) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(dunningConfiguration.getDateFormat());
        LocalDate startDate = LocalDate.parse(fromBillDate, formatter);
        LocalDate endDate = LocalDate.parse(toBillDate, formatter);

        long noOfDaysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        if(noOfDaysBetween <= Long.valueOf(dunningConfiguration.getFetchInvoiceInDays())) {
            return true;
        } else {
            return false;
        }
    }

    public ImmutablePair<Date, Date> getDates() {

        Date currentDate = new Date();
        Date lastDate = new Date(currentDate.getTime() - Long.valueOf(dunningConfiguration.getFetchInvoiceInDays()) * 24 * 3600 * 1000L);

        System.out.println("Current Date is: " + currentDate.toString());
        System.out.println("Last One Year Date is: " + lastDate);

        return ImmutablePair.of(lastDate, currentDate);
    }
}
