package com.mavenir.billing.bc.controller;

import com.mavenir.billing.bc.exception.ControllerExceptionResponse;
import com.mavenir.billing.bc.exception.Error;
import com.mavenir.billing.bc.exception.ServiceException;
import com.mavenir.billing.bc.model.InvoiceResponse;
import com.mavenir.billing.bc.service.InvoiceService;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import java.util.List;
import java.util.Optional;

@Slf4j
@RequiredArgsConstructor
@RestController
public class InvoiceController {

    @Autowired
    InvoiceService invoiceService;

    @GetMapping("/billing/invoiceHistory")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = " Success"),
            @ApiResponse(code = 404, message = "Account/Document does not exist", response = ControllerExceptionResponse.class),
            @ApiResponse(code = 400, message = "Invalid Request", response = ControllerExceptionResponse.class)
    })
    public ResponseEntity<List<InvoiceResponse>> executeInvoice(
            @RequestParam (required = true) String accountId,
            @RequestParam (required = false) String fromBillDate,
            @RequestParam (required = false) String toBillDate) {

        log.info("Invoice Controller starts");
        List<InvoiceResponse> invoices = null;
        if (!Optional.ofNullable(accountId).isEmpty() &&
                (Optional.ofNullable(fromBillDate).isEmpty() && Optional.ofNullable(toBillDate).isEmpty())) {
            invoices =  invoiceService.getHistoryInvoicesByAccountId(accountId);
            return new ResponseEntity(invoices, HttpStatus.OK);
        } else if (!Optional.ofNullable(accountId).isEmpty() &&
                (!Optional.ofNullable(fromBillDate).isEmpty() && !Optional.ofNullable(toBillDate).isEmpty())) {
            invoices = invoiceService.getHistoryInvoicesByDates(accountId, fromBillDate, toBillDate);
            return new ResponseEntity(invoices, HttpStatus.OK);
        } else {
            throw new ServiceException("Invalid Request", Error.INVALID_REQUEST);
        }
    }
}
