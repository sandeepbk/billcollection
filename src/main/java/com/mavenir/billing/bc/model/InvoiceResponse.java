package com.mavenir.billing.bc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.couchbase.core.mapping.Document;

import java.util.Date;
import java.util.Locale;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class InvoiceResponse implements Comparable {
    private String invoiceNumber;
    private String date;
    private Double amountPaid;
    private Double balance;
    private String status;

    @Override
    public int compareTo(Object object) {
        return this.getInvoiceNumber().compareTo(((InvoiceResponse)object).getInvoiceNumber());
    }
}
