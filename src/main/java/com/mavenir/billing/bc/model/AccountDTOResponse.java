package com.mavenir.billing.bc.model;

import java.util.List;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@ToString
public class AccountDTOResponse {
    private String id;
    private String type;
    private String companyName;
    private List<AccountEmailDTOResponse> emails;
    private AccountNameDTOResponse name;
    private List<AccountLocaleDTOResponse> locales;
}
