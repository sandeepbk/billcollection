package com.mavenir.billing.bc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@ToString
public class AccountPhoneDTOResponse {
	private String type;
	private String number;
	private Boolean verified;
	private String verificationCode;
	private String note;	
	private String extn;

}
