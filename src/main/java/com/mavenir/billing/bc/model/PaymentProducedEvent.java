package com.mavenir.billing.bc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PaymentProducedEvent {
    private String bssBillingId;
    private String bssTransactionId;
    private String bssAccessTime;
    private String bkPaymentTransactionId;
    private String bkPaymentDateandTime;
    private Double bkPaymentAmount;
    private String bssDescription;
    private String bssGLCode;
}
