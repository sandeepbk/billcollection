package com.mavenir.billing.bc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@ToString
public class AccountAddressDTOResponse {
    private String type;
    private String departmentName;
    private String line1;
    private String line2;
    private String city;
    private String stateORprovince;
    private String postalCode;
    private String country;
    private Boolean verified;
    private String verificationCode;
    private String note;

}
