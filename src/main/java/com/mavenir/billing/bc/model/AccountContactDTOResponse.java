package com.mavenir.billing.bc.model;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import lombok.Data;

/**
 * Contact
 *
 */

@Data
public class AccountContactDTOResponse {

	@NotNull
	private String type;

	private String designation;

	@Valid
	private AccountNameDTOResponse name;

	@Valid
	private List<AccountPhoneDTOResponse> phones = new ArrayList<AccountPhoneDTOResponse>();

	@Valid
	private List<AccountEmailDTOResponse> emails = new ArrayList<AccountEmailDTOResponse>();

	private String note;

}
