package com.mavenir.billing.bc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class CollectionDocument {
    private String referenceId;
    private String billingAccId;
    private String event;
    private String dunningBillDueDate;
    private String invoiceId;
}