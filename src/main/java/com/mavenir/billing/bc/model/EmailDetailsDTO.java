package com.mavenir.billing.bc.model;

import java.util.List;
import java.util.Map;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;
import lombok.Value;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class EmailDetailsDTO {

    private String from;
    private List<String> to;
    private List<String> cc;
    private List<String> bcc;
    private String entityType;
    private String entityId;
    private String companyName;
    private String accountId;

    private Map<String, String> templateParams;
}
