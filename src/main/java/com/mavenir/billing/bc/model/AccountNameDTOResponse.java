package com.mavenir.billing.bc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@ToString
public class AccountNameDTOResponse {
    private String namePrefix;

    private String firstName;

    private String middleName;

    private String lastName;

    private String nameSuffix;

    @JsonIgnore
    public String getSimpleName() {
            return this.firstName
                            + ((this.lastName != null) ? " " + this.lastName : "");
    }

    @JsonIgnore
    public String getFullName() {
            return this.firstName
                            + ((this.middleName != null) ? " " + this.middleName : "")
                            + ((this.lastName != null) ? " " + this.lastName : "");
    }
}
