package com.mavenir.billing.bc.model;

import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;

import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@ToString
public class EmailDTOResponse {
    private String id;
    private List<String> to;
    private List<String> cc;
    private List<String> bcc;
    private String entityType;
    private String entityId;
    private Date sentTime;
}
