package com.mavenir.billing.bc.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@JsonIgnoreProperties(ignoreUnknown=true)
public class Billing {

    @NotBlank
    private String billingAccId;
    private String ktab;
    private String version;
    private String dunningProfileId;
    private String accountState;
    private Double suspenseBal;
    private List<DueBill> dueBill;
    private List<DueBill> historyBill;
}