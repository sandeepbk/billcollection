package com.mavenir.billing.bc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
public class EmailBillingInfo {
    private String accountId;
    private String destEmailAddress;
    private String entityType;
    private String entityId;
    private String companyName;
    private String customerName;
}
