package com.mavenir.billing.bc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class DueBill {
    private String invoiceId;
    private Date billDate;
    private Date dueDate;
    private Double dueAmount;
    private Double outstandingAmount;
    private String schedulerReferenceId;
    private String billState;
    private String serviceState;
    private Integer billHashCode;
}
