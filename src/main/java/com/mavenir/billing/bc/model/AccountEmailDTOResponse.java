package com.mavenir.billing.bc.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@ToString
public class AccountEmailDTOResponse {
    private String type;
    private String address;
    private Boolean verified;
    private String note;
}
