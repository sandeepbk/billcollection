package com.mavenir.billing.bc.model;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@ToString
public class AccountLocaleDTOResponse {
    private String language;
    private String companyName;
    private AccountContactDTOResponse contacts;
    private AccountNameDTOResponse name;
    private List<AccountAddressDTOResponse> addressess;
}
