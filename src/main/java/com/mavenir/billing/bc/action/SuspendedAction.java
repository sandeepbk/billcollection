package com.mavenir.billing.bc.action;

import com.mavenir.billing.bc.config.BillCollectionCsvConfiguration;
import com.mavenir.billing.bc.config.CsvConfiguration;
import com.mavenir.billing.bc.config.GitRepoConfiguration;
import com.mavenir.billing.bc.constants.BillCollectionConstants;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.dep.common.profileexecutor.Action;
import com.mavenir.dep.common.profileexecutor.ActionExecutionException;
import com.rakuten.billing.bc.utils.CsvGenerator;
import com.rakuten.billing.bc.utils.model.ServiceSuspendRecord;
import com.rakuten.billing.gitmgr.model.GitCredentials;
import com.rakuten.billing.gitmgr.model.RepositoryDetails;
import com.rakuten.billing.gitmgr.model.SshCredentials;
import com.rakuten.billing.gitmgr.utils.GitRepositoryMgr;

import lombok.extern.slf4j.Slf4j;

import org.eclipse.jgit.api.Git;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class SuspendedAction implements Action {

    @Autowired
    BillCollectionCsvConfiguration billCollectionCsvConfiguration;
    
    @Autowired
    GitRepoConfiguration gitRepoConfiguration;

    @Override
    public void execute(Object o, Object d, Map map) throws ActionExecutionException {
        Billing billing = (Billing) o;
        ServiceSuspendRecord record = new ServiceSuspendRecord();
        record.setAccountId(billing.getBillingAccId());
        DueBill dueBill = (DueBill) d;

        if (dueBill != null) {
            record.setAccountAsIsState(dueBill.getBillState());
        }

        record.setAccountToBeState(BillCollectionConstants.SUSPENDED);
        record.setDunningState(BillCollectionConstants.DUNNING_SUSPENDED);

        try {
            log.info("Initializing git repo...");
            RepositoryDetails repository = new RepositoryDetails();
            repository.setUri(gitRepoConfiguration.getUri());
            repository.setDirectory(gitRepoConfiguration.getLocalDirectory());

            SshCredentials credentials = new SshCredentials();
            credentials.setApplicationId(gitRepoConfiguration.getApplicationId());
            credentials.setPrivateKey(gitRepoConfiguration.getPrivateKey());
            credentials.setPassPhrase(gitRepoConfiguration.getPassPhrase());
            
            log.info("Getting an instance of GitRepositoryMgr...");
            GitRepositoryMgr gitRepositoryMgr = GitRepositoryMgr.getInstance();
            
            log.info("Initialize SSH Credentials...");
            gitRepositoryMgr.initializeSshCredentials(credentials);
            
            log.info("Checking local folder...");
            if (!gitRepositoryMgr.isLocalFolderExist(repository)) {
                log.info("create new folder");
                gitRepositoryMgr.createLocalDirectory(repository.getDirectory());
            } else {
                log.info("local folder already exist");
            }

            log.info("Checking local repository...");
            if (!gitRepositoryMgr.isLocalRepositoryValid(repository)) {
                log.info("creating new clone");
                gitRepositoryMgr.cloneRepository(repository, credentials);
            } else {
                log.info("local Repository is valid...");
            }

            log.info("creating csv file...");
            CsvConfiguration csvConfiguration = billCollectionCsvConfiguration.getCsvConfiguration();
            log.info("suspended record {}", record);
            log.info("path {} prefix name {} extension {}", csvConfiguration.getPath(), csvConfiguration.getSuspendedPrefixName(),
                    csvConfiguration.getExtName());
            CsvGenerator.createAppendCsvFile(csvConfiguration.getPath(), csvConfiguration.getSuspendedPrefixName(),
                    BillCollectionConstants.DATE_FORMAT_YYYYMMDD, csvConfiguration.getExtName(), record);

            log.info("add new files, commit and push file changes to remote repository...");
            Git git = gitRepositoryMgr.addCommitAndPushAllFileChanges(repository, credentials);

            log.info("closing local repository...");
            git.close();

            log.info("Done.");

        } catch (Exception e) {
            log.error("An error occurred while writing record in " + "Suspended Action CSV File for billing account id {}",
                    billing.getBillingAccId());
            throw new ActionExecutionException(e.getMessage(), e);
        }
        log.info("Suspended action called for billing account id {}", billing.getBillingAccId());
    }
}
