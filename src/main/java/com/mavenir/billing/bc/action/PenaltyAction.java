package com.mavenir.billing.bc.action;

import com.mavenir.billing.bc.client.CollectionAccountClient;
import com.mavenir.billing.bc.constants.BillCollectionConstants;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.dep.common.profileexecutor.Action;
import com.mavenir.dep.common.profileexecutor.ActionExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class PenaltyAction implements Action {

    @Autowired
    private CollectionAccountClient collectionAccountClient;

    @Override
    public void execute(Object o, Object d, Map map) throws ActionExecutionException {
        Billing billing = (Billing) o;
        String penaltyString = (String) map.get(BillCollectionConstants.PENALTY_AMOUNT);
        if (penaltyString != null) {
            log.info("Penalty amount {} added ", penaltyString);
            int statusCode = collectionAccountClient.addPenalty(billing.getBillingAccId(), Double.parseDouble(penaltyString));
        }
        log.info("Penalty action called for billing account id {}",billing.getBillingAccId());
    }
}
