package com.mavenir.billing.bc.action;


import com.mavenir.billing.bc.model.Billing;
import com.mavenir.dep.common.profileexecutor.Action;
import com.mavenir.dep.common.profileexecutor.ActionExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Map;

@Slf4j
@Component
public class ScheduleAction implements Action {
    @Override
    public void execute(Object o, Object d, Map map) throws ActionExecutionException {
        Billing billing = (Billing) o;
        log.info("Schedule action called for billing account id {}",billing.getBillingAccId());
    }
}
