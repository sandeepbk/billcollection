package com.mavenir.billing.bc.action;

import com.mavenir.billing.bc.constants.BillCollectionConstants;
import com.mavenir.billing.bc.exception.ProcessException;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.bc.model.EmailWithTemplateDTO;
import com.mavenir.billing.bc.service.BillAccountMgmtService;
import com.mavenir.billing.bc.service.EmailNotificationService;
import com.mavenir.billing.bc.model.EmailBillingInfo;
import com.mavenir.billing.bc.model.EmailDTOResponse;
import com.mavenir.billing.bc.model.EmailDetailsDTO;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.dep.common.profileexecutor.Action;
import com.mavenir.dep.common.profileexecutor.ActionExecutionException;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestClientException;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@Component
@AllArgsConstructor
@RequiredArgsConstructor
public class SendEmailAction implements Action {

    @Autowired
    EmailNotificationService emailNotificationService;

    @Autowired
    BillAccountMgmtService billAccountMgmtService;

    @Autowired
    BillingRepository billingRepository;

    @SneakyThrows
    @Override
    public void execute(Object o, Object d, Map map) throws ActionExecutionException {
        Billing billing = (Billing) o;
        DueBill dueBill = (DueBill) d;

        log.info("Email sending action called for billing account id {}", billing.getBillingAccId());

        EmailBillingInfo emailBillingInfo = null;
        log.info("EmailBillingInfo : {}", emailBillingInfo);
        try {
            emailBillingInfo = getEmailBillingInfoId(billing.getBillingAccId());
        } catch (Exception e) {
            log.error("An error occured while invoking Account Management Service for billing account id {}",
                    billing.getBillingAccId());
            throw new ActionExecutionException(e.getMessage(), e);
        }

        String emailTemplate = (String) map.get(BillCollectionConstants.EMAIL_TEMPLATE);
        if (emailTemplate != null) {
            log.info("Email template name is {} ", emailTemplate);
        }

        // Map EmailWithTemplateDTO object with Billing, DueBill, EmailBillingInfo and
        // Map
        EmailWithTemplateDTO request = new EmailWithTemplateDTO();
        EmailDetailsDTO email = new EmailDetailsDTO();

        // to
        if (emailBillingInfo.getDestEmailAddress() != null) {
            List<String> to = Arrays.asList(emailBillingInfo.getDestEmailAddress());
            email.setTo(to);
        }

        // entityType
        email.setEntityType(emailBillingInfo.getEntityType());

        // entityId
        email.setEntityId(emailBillingInfo.getEntityId());

        // companyName
        email.setCompanyName(emailBillingInfo.getCompanyName());

        // accountId
        email.setAccountId(emailBillingInfo.getAccountId());

        // templateParams
        Map<String, String> templateParams = new HashMap<String, String>();
        SimpleDateFormat sdfYear = new SimpleDateFormat(BillCollectionConstants.DATE_FORMAT_YEAR);
        SimpleDateFormat sdfMonth = new SimpleDateFormat(BillCollectionConstants.DATE_FORMAT_MONTH);
        SimpleDateFormat sdfDay = new SimpleDateFormat(BillCollectionConstants.DATE_FORMAT_DAY);

        // customerName
        templateParams.put("customerName", emailBillingInfo.getCustomerName());

        // date_year
        templateParams.put("date_year", sdfYear.format(dueBill.getDueDate()));

        // date_month
        templateParams.put("date_month", sdfMonth.format(dueBill.getDueDate()));

        // date_day
        templateParams.put("date_day", sdfDay.format(dueBill.getDueDate()));

        // fee
        templateParams.put("fee", String.valueOf(dueBill.getDueAmount()));

        // billingId
        templateParams.put("billingId", emailBillingInfo.getAccountId());

        // billingMonth_year[0]
        templateParams.put("billingMonth_year[0]", sdfYear.format(dueBill.getBillDate()));

        // billingMonth_month[0]
        templateParams.put("billingMonth_month[0]", sdfMonth.format(dueBill.getBillDate()));

        // billingMonth - 1 month
        Calendar c = Calendar.getInstance();
        c.setTime(dueBill.getBillDate());
        c.add(Calendar.MONTH, -1);

        // usageMonth_year[0]
        templateParams.put("usageMonth_year[0]", sdfYear.format(c.getTime()));

        // usageMonth_month[0]
        templateParams.put("usageMonth_month[0]", sdfMonth.format(c.getTime()));

        // For Dunning2 only
        // usageMonth_year[1]
        // usageMonth_month[1]

        // billingMonth_year[1]
        // billingMonth_month[1]

        email.setTemplateParams(templateParams);

        request.setEmail(email);

        try {
            EmailDTOResponse response = emailNotificationService.sendNotificationEmail(request, emailTemplate);
            log.info(response.toString());
        } catch (Exception e) {
            log.error("An error occured while invoking Email Notificaiton Service for billing account id {}",
                    billing.getBillingAccId());
            throw new ActionExecutionException(e.getMessage(), e);
        }

    }

    /**
     * @param billingId "Billing Account id"
     * @return Billing "Billing object of Billing Account id"
     * @throws ProcessException         "If Billing Account id is not found,
     *                                  ProcessException is thrown"
     * @throws KeyStoreException
     * @throws NoSuchAlgorithmException
     * @throws KeyManagementException
     * @throws RestClientException
     */
    public EmailBillingInfo getEmailBillingInfoId(String billingId)
            throws ProcessException, RestClientException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        EmailBillingInfo emailBillingInfo = billAccountMgmtService.getEmailBillingInfo(billingId);
        if (emailBillingInfo != null) {
            return emailBillingInfo;
        } else {
            log.info("Account Id doesn't exist");
            throw new ProcessException("Account Id doesn't exist");
        }
    }
}