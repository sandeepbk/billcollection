package com.mavenir.billing.bc.service;

import com.mavenir.billing.bc.client.CollectionSchedulerClient;
import com.mavenir.billing.bc.config.BillCollectionDunningConfiguration;
import com.mavenir.billing.bc.config.DunningConfiguration;
import com.mavenir.billing.bc.constants.BillCollectionConstants;
import com.mavenir.billing.bc.exception.ProcessException;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.billing.scheduler.grpc.Invoice;
import com.mavenir.dep.common.profileexecutor.ProfileExecutorLibrary;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Component
@AllArgsConstructor
@RequiredArgsConstructor
public class BillDunningService {

    @Autowired
    DunningConfiguration dunningConfiguration;

    @Autowired
    private CollectionSchedulerClient collectionSchedulerClient;

    @Autowired
    private BillingRepository billingRepository;

    @Autowired
    private BillCollectionDunningConfiguration billCollectionDunningConfiguration;

    /**
     * @param billingId "Billing Account id"
     * @return Billing      "Billing object of Billing Account id"
     * @throws ProcessException "If Billing Account id is not found, ProcessException is thrown"
     */
    public Billing getBillingIdFromDB(String billingId) throws ProcessException {
        String documentId = dunningConfiguration.getDocumentID().concat(billingId);
        Optional<Billing> billingOptional = billingRepository.findById(documentId);
        if (billingOptional.stream().findFirst().isPresent()) {
            return billingOptional.stream().findFirst().get();
        } else {
            log.info("Account Id doesn't exist");
            throw new ProcessException("Account Id doesn't exist");
        }
    }

    /**
     * @param billing "Billing data for account id"
     * @param dueBill "Invoice bill data"
     * @param event   "Current event"
     */
    public void processDunningEvent(Billing billing, DueBill dueBill, String event) {
        ProfileExecutorLibrary<String, String, Billing, DueBill> profileExecutorLibrary =
                billCollectionDunningConfiguration.getProfileExecutorLibrary();
        Map<String, String> map = profileExecutorLibrary.getActionData(billing.getDunningProfileId(),
                dueBill.getBillState().toUpperCase(), event.toUpperCase(),
                BillCollectionConstants.SCHEDULE);
        log.info("Map value {}", map);
        if (map != null) {
            Date billingDat = dueBill.getBillDate();
            LocalDate billingDate = billingDat.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
            DateTimeFormatter formatter = DateTimeFormatter.ofPattern(BillCollectionConstants.DATE_FORMAT_DDMMYYYY);
            String monthOffsetStr = map.get(BillCollectionConstants.MONTH_OFFSET);
            String plusDaysStr = map.get(BillCollectionConstants.PLUS_DAYS);
            String minusDaysStr = map.get(BillCollectionConstants.MINUS_DAYS);
            if (monthOffsetStr != null) {
                if (plusDaysStr != null && minusDaysStr != null) {
                    int monthOffset = (!monthOffsetStr.equals("")) ? Integer.parseInt(monthOffsetStr) : 0;
                    int plusDays = (!plusDaysStr.equals("")) ? Integer.parseInt(plusDaysStr) : 0;
                    int minusDays = (!minusDaysStr.equals("")) ? Integer.parseInt(minusDaysStr) : 0;
                    log.info("billingDueDate BEFORE {}", billingDate);
                    LocalDate billingDueLocalDate = billingDate.plusMonths(monthOffset).plusDays(plusDays).minusDays(minusDays);
                    String billingDueDateStr = formatter.format(billingDueLocalDate);
                    log.info("billingDueDate AFTER {}", billingDueDateStr);
                    String nextEvent = map.get(BillCollectionConstants.NEXT_EVENT);
                    List<Invoice> invoiceList = collectionSchedulerClient.addAccFromDunning(billing.getBillingAccId(),
                            dueBill.getInvoiceId(), nextEvent.toUpperCase(), billingDueDateStr);
                    if (invoiceList != null) {
                        for (Invoice invoice : invoiceList) {
                            log.info("Scheduler reference ID {}", invoice.getReferenceId());
                            dueBill.setSchedulerReferenceId(invoice.getReferenceId());
                            dueBill.setDueDate(Date.from(billingDueLocalDate.atStartOfDay(ZoneId.systemDefault()).toInstant()));
                        }
                    }
                }
            }
        }
    }
}
