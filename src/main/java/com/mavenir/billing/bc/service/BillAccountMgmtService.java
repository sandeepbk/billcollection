package com.mavenir.billing.bc.service;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.mavenir.billing.bc.config.BillAccountMgmtConfiguration;
import com.mavenir.billing.bc.constants.BillCollectionConstants;
import com.mavenir.billing.bc.model.AccountDTOResponse;
import com.mavenir.billing.bc.model.AccountEmailDTOResponse;
import com.mavenir.billing.bc.model.AccountLocaleDTOResponse;
import com.mavenir.billing.bc.model.EmailBillingInfo;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class BillAccountMgmtService {
    RestTemplate restTemplate;

    @Autowired
    BillAccountMgmtConfiguration acctMgmtConfig;

    /**
     * This method connects to Account Management Service via Restful API call
     * 
     * @param accountId "account id"
     * @return EmailBillingInfo "response"
     * @throws RestClientException         "Exception handling"
     * @throws KeyManagementException      "Exception handling"
     * @throws NoSuchAlgorithmException    "Exception handling"
     * @throws KeyStoreException"Exception handling"
     */
    public EmailBillingInfo getEmailBillingInfo(String accountId)
            throws RestClientException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {

        if (restTemplate == null) {
            createRequestFactory();
        }

        String url = acctMgmtConfig.getConnectionAddress() + ":" + acctMgmtConfig.getPort() + acctMgmtConfig.getContextPath();
        url = url.replace("{accountId}", accountId);

        try {
            ResponseEntity<AccountDTOResponse> response = restTemplate.getForEntity(url, AccountDTOResponse.class);

            if (response.getStatusCode().equals(HttpStatus.OK)) {
                log.info("Bill Account Management Service returned status code: 200 (OK)");
                EmailBillingInfo emailBillingInfo = new EmailBillingInfo();

                // Perform mapping between AccountDTOResponse and EmailBillingInfo
                emailBillingInfo.setAccountId(response.getBody().getId());
                emailBillingInfo.setCompanyName(response.getBody().getCompanyName());
                emailBillingInfo.setEntityId(response.getBody().getId());
                emailBillingInfo.setEntityType(response.getBody().getType());

                if (response.getBody().getEmails() != null && response.getBody().getEmails().size() > 0) {
                    for (AccountEmailDTOResponse email : response.getBody().getEmails()) {
                        if (email.getType().equals(BillCollectionConstants.PRIMARY_EMAIL_TYPE)) {
                            emailBillingInfo.setDestEmailAddress(email.getAddress());
                        }
                    }
                }

                if (response.getBody().getLocales() != null && response.getBody().getLocales().size() > 0) {
                    for (AccountLocaleDTOResponse locale : response.getBody().getLocales()) {
                        if (locale.getName() != null) {
                            emailBillingInfo.setCustomerName(locale.getName().getSimpleName());
                        } else {
                            emailBillingInfo.setCustomerName(response.getBody().getName().getSimpleName());
                        }
                    }
                }

                System.out.println("EmailBillingInfo:" + emailBillingInfo.toString());
                return emailBillingInfo;
            } else {
                log.error("Bill Account Management Service returned status code: " + response.getStatusCode());
            }
        } catch (RestClientException e) {
            log.error("Unable to connect to Bill Account Management Service.");
            throw e;
        }

        log.error("No Bill Account Management Service Response data provided.");
        return null;
    }

    /**
     * This method creates request Factory for RestTemplate (specifically for https
     * connections)
     * 
     * @throws KeyManagementException      "Exception handling"
     * @throws NoSuchAlgorithmException    "Exception handling"
     * @throws KeyStoreException"Exception handling"
     */
    public void createRequestFactory() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        if (acctMgmtConfig.getSslFlag()) {
            TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;

            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();

            // NoopHostnameVerifier is temporary until certificate is provided
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();

            BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(socketFactoryRegistry);

            CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
                    .setConnectionManager(connectionManager).build();

            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

            restTemplate = new RestTemplate(requestFactory);
        } else {
            restTemplate = new RestTemplate();
        }
    }
}
