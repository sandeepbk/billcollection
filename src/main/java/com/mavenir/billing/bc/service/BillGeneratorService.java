package com.mavenir.billing.bc.service;

import com.mavenir.billing.bc.config.BillCollectionDunningConfiguration;
import com.mavenir.billing.bc.config.DunningConfiguration;
import com.mavenir.billing.bc.constants.BillCollectionConstants;
import com.mavenir.billing.bc.exception.ProcessException;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.billing.generator.grpc.BillCollectionGeneratorServiceGrpc;
import com.mavenir.billing.generator.grpc.BillingCollectionRequest;
import com.mavenir.billing.generator.grpc.BillingCollectionResponse;
import com.mavenir.dep.common.profileexecutor.ProfileExecutionException;
import com.mavenir.dep.common.profileexecutor.ProfileExecutorInitializationException;
import com.mavenir.dep.common.profileexecutor.ProfileExecutorLibrary;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@GRpcService
@AllArgsConstructor
@RequiredArgsConstructor
public class BillGeneratorService extends BillCollectionGeneratorServiceGrpc.BillCollectionGeneratorServiceImplBase {

    @Value("${bc.ktab}")
    private String ktab;

    @Autowired
    private BillingRepository billingRepository;

    @Autowired
    private BillDunningService billDunningService;

    @Autowired
    private DunningConfiguration dunningConfiguration;

    @Autowired
    private BillCollectionDunningConfiguration billCollectionDunningConfiguration;

    /**
     * @param billingCollectionRequest  Request from Bill generation GRPC
     * @param responseStreamObserver    Response to Bill generation GRPC
     */
    @Override
    public void generateBill(BillingCollectionRequest billingCollectionRequest,
                             StreamObserver<BillingCollectionResponse> responseStreamObserver) {
        log.debug("Billing account id {}", billingCollectionRequest.getBillingAccId());
        Billing billing = new Billing();
        boolean skipDunningFlag = false;
        try {
            billing = billDunningService.getBillingIdFromDB(billingCollectionRequest.getBillingAccId());
            if(billingCollectionRequest.getDueAmount() < 0){
                billing.setSuspenseBal(Math.abs(billingCollectionRequest.getDueAmount()));
            } else {
                billing.setSuspenseBal(0.0);
            }
        } catch (ProcessException e) {
            billing.setBillingAccId(billingCollectionRequest.getBillingAccId());
            billing.setKtab(ktab);
            billing.setAccountState(BillCollectionConstants.ACTIVE);
            billing.setDunningProfileId(billingCollectionRequest.getDunningProfileId());
        }
        DateFormat formatter = new SimpleDateFormat(BillCollectionConstants.DATE_FORMAT_DDMMYYYY);
        DueBill dueBill = null;
        try {
            String firstState = getState(billing.getDunningProfileId()).stream().findFirst().get();
            dueBill = DueBill.builder().
                    billDate(formatter.parse(billingCollectionRequest.getBillDate())).
                    dueAmount(billingCollectionRequest.getDueAmount()).
                    outstandingAmount(billingCollectionRequest.getDueAmount()).
                    invoiceId(billingCollectionRequest.getInvoiceNumber()).
                    billState(firstState).build();
            if(dueBill.getDueAmount() <= 0) {
                skipDunningFlag = true;
                dueBill.setOutstandingAmount(0.0);
                if(billing.getHistoryBill()!= null){
                    billing.getHistoryBill().add(dueBill);
                } else {
                    List<DueBill> list = new ArrayList();
                    list.add(dueBill);
                    billing.setHistoryBill(list);
                }
            }
        } catch (ParseException | ProfileExecutionException e) {
            log.error("Invoice data processing failed",e);
        }
        if(!skipDunningFlag) {
            ProfileExecutorLibrary<String, String, Billing, DueBill> profileExecutorLibrary =
                    billCollectionDunningConfiguration.getProfileExecutorLibrary();
            String firstEvent = null;
            try {
                firstEvent = getEvent(billing.getDunningProfileId()).stream().findFirst().get();
            } catch (ProfileExecutionException e) {
                log.error("Event extraction failed", e);
            }
            billDunningService.processDunningEvent(billing, dueBill, firstEvent);
            String nextState = null;
            try {
                nextState = profileExecutorLibrary.processEvent(billing.getDunningProfileId(),
                        dueBill.getBillState(), firstEvent, billing, dueBill);
            } catch (ProfileExecutionException | ProfileExecutorInitializationException e) {
                log.error("Processing next state failed", e);
            }
            log.info("State changed from {} to {}", dueBill.getBillState(), nextState);
            dueBill.setBillState(nextState);
            dueBill.setServiceState(nextState);
            if (billing.getDueBill() != null) {
                billing.getDueBill().add(dueBill);
            } else {
                List<DueBill> dueBillList = new ArrayList<>();
                dueBillList.add(dueBill);
                billing.setDueBill(dueBillList);
            }
        }
        billingRepository.save(billing, dunningConfiguration.getDocumentID().concat(billing.getBillingAccId()));
        log.info("Generated Bill is added to Billing account");
        BillingCollectionResponse billingCollectionResponse = BillingCollectionResponse.newBuilder().
                setStatusCode(BillCollectionConstants.OK_200).build();
        responseStreamObserver.onNext(billingCollectionResponse);
        responseStreamObserver.onCompleted();
        log.info("Bill generated for the account {}", billingCollectionRequest.getBillingAccId());
    }

    private List<String> getState(String profileId) throws ProfileExecutionException {
        Map<String, Map<Integer, String>> stateMap = billCollectionDunningConfiguration.getStateMap();
        if (profileId != null) {
            Map<Integer, String> profileMap = stateMap.get(profileId);
            if (profileMap != null) {
                return new ArrayList<>(profileMap.values());
            }
        }
        return matchNotFound(profileId);
    }

    private List<String> getEvent(String profileId) throws ProfileExecutionException {
        Map<String, Map<Integer, String>> eventMap = billCollectionDunningConfiguration.getEventMap();
        if (profileId != null) {
            Map<Integer, String> profileMap = eventMap.get(profileId);
            if (profileMap != null) {
                return new ArrayList<>(profileMap.values());
            }
        }
        return matchNotFound(profileId);
    }

    private List<String> matchNotFound(String profileId) throws ProfileExecutionException {
        String msg = String.format("%s profile not found", profileId);
        log.warn(msg);
        throw new ProfileExecutionException(msg);
    }
}
