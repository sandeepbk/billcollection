package com.mavenir.billing.bc.service;

import com.mavenir.billing.bc.client.CollectionSchedulerClient;
import com.mavenir.billing.bc.config.BillCollectionDunningConfiguration;
import com.mavenir.billing.bc.config.DunningConfiguration;
import com.mavenir.billing.bc.constants.BillCollectionConstants;
import com.mavenir.billing.bc.exception.ProcessException;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.bc.model.PaymentProducedEvent;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.dep.common.profileexecutor.ProfileExecutionException;
import com.mavenir.dep.common.profileexecutor.ProfileExecutorInitializationException;
import com.mavenir.dep.common.profileexecutor.ProfileExecutorLibrary;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
@AllArgsConstructor
@RequiredArgsConstructor
public class BillPaymentService {

    @Autowired
    private CollectionSchedulerClient collectionSchedulerClient;

    @Autowired
    private BillDunningService billDunningService;

    @Autowired
    private BillingRepository billingRepository;

    @Autowired
    private BillCollectionDunningConfiguration billCollectionDunningConfiguration;

    @Autowired
    private DunningConfiguration dunningConfiguration;

    /**
     * @param paymentProducedEvent "Payment gateway data"
     * @throws ProcessException                       "Exception handled"
     * @throws ProfileExecutorInitializationException "Exception handled"
     * @throws ProfileExecutionException              "Exception handled"
     */
    public void dunningEventRemoval(PaymentProducedEvent paymentProducedEvent) throws ProcessException,
            ProfileExecutorInitializationException, ProfileExecutionException {
        ProfileExecutorLibrary<String, String, Billing, DueBill> profileExecutorLibrary =
                billCollectionDunningConfiguration.getProfileExecutorLibrary();
        Billing billing = billDunningService.getBillingIdFromDB(paymentProducedEvent.getBssBillingId());
        if (billing.getDueBill().size() > 0) {
            billing.getDueBill().sort(new SortByBillDate());
        }
        Integer billHashCode = billing.getDueBill().hashCode();
        boolean paymentSuccess = deleteBillPaidData(billing, paymentProducedEvent.getBkPaymentAmount(), billHashCode);
        String schedulerResponse = BillCollectionConstants.EMPTY;
        if (paymentSuccess) {
            List<DueBill> dueBillList = billing.getHistoryBill().stream().filter(bill ->
                    billHashCode.equals(bill.getBillHashCode())).collect(Collectors.toList());
            for (DueBill dueBill : dueBillList) {
                String state = getState(billing.getDunningProfileId()).get(1);
                if (!state.equals(dueBill.getBillState())) {
                    String nextState = profileExecutorLibrary.processEvent(billing.getDunningProfileId(),
                            dueBill.getBillState().toUpperCase(), BillCollectionConstants.FULL_PAYMENT, billing, dueBill);
                    log.info("State changed from {} to {}", dueBill.getBillState(), nextState);
                    List<DueBill> billList = billing.getHistoryBill().stream().peek(bill -> {
                        if (billHashCode.equals(bill.getBillHashCode())) {
                            bill.setBillState(nextState);
                        }
                    }).collect(Collectors.toList());
                    billing.setHistoryBill(billList);
                }
            }
            schedulerResponse = collectionSchedulerClient.deleteAccFromDunning(billing, dueBillList);
        } else {
            schedulerResponse = BillCollectionConstants.NA;
        }
        if (BillCollectionConstants.SUCCESS.equals(schedulerResponse) ||
                BillCollectionConstants.NA.equals(schedulerResponse)) {
            billingRepository.save(billing, dunningConfiguration.getDocumentID().concat(billing.getBillingAccId()));
        } else {
            throw new ProcessException("Deleting reference ID failed");
        }
    }

    /**
     * @param billing    "Billing data"
     * @param paidAmount "Amount paid for Billing account ID"
     * @return paymentSuccess   "True only after successfully paid for the oldest invoice in due bills"
     */
    public boolean deleteBillPaidData(Billing billing, double paidAmount, int billHashCode) {
        boolean paymentSuccess = false;
        if (billing.getDueBill() != null && billing.getDueBill().size() > 0) {
            paymentSuccess = fullPaymentMadeToDueBill(billing, paidAmount, billHashCode);
        } else {
            double balance = billing.getSuspenseBal();
            billing.setSuspenseBal(balance + paidAmount);
        }
        if (paymentSuccess) {
            if (billing.getDueBill().stream().findFirst().isPresent()) {
                billing.setAccountState(billing.getDueBill().stream().findFirst().get().getServiceState());
            } else {
                billing.setAccountState(BillCollectionConstants.ACTIVE);
            }
        }
        return paymentSuccess;
    }

    private boolean fullPaymentMadeToDueBill(Billing billing, double paidAmount, int billHashCode) {
        boolean paymentSuccess = false;
        double balanceAmount = 0;
        double suspenseBalance = 0;
        if (billing.getSuspenseBal() != null) {
            balanceAmount = billing.getSuspenseBal();
        }
        balanceAmount += paidAmount;
        List<DueBill> removedDueBillList = new ArrayList<>();
        //List<DueBill> dueBillList = new ArrayList<>();
        for (DueBill dueBill : billing.getDueBill()) {
            Double outstandingAmount = dueBill.getOutstandingAmount();
            if (balanceAmount >= outstandingAmount) {
                dueBill.setBillHashCode(billHashCode);
                if (outstandingAmount >= balanceAmount) {
                    dueBill.setOutstandingAmount(outstandingAmount - balanceAmount);
                } else {
                    dueBill.setOutstandingAmount(0.0);
                    suspenseBalance = balanceAmount - outstandingAmount;
                }
                if (billing.getHistoryBill() == null) {
                    List<DueBill> historyBillList = new ArrayList<>();
                    historyBillList.add(dueBill);
                    billing.setHistoryBill(historyBillList);
                } else {
                    billing.getHistoryBill().add(dueBill);
                }
                removedDueBillList.add(dueBill);
                paymentSuccess = true;
            } else {
                dueBill.setOutstandingAmount(outstandingAmount - balanceAmount);
                //dueBillList.add(dueBill);
            }
        }
        billing.getDueBill().removeAll(removedDueBillList);
        if (billing.getDueBill().size() == 0) {
            billing.setSuspenseBal(suspenseBalance);
        }
        return paymentSuccess;
    }

    /**
     * @param profileId     Collection profile ID
     * @return List         list of state
     * @throws ProfileExecutionException    "Exception handled"
     */
    private List<String> getState(String profileId) throws ProfileExecutionException {
        Map<String, Map<Integer, String>> stateMap = billCollectionDunningConfiguration.getStateMap();
        if (profileId != null) {
            Map<Integer, String> profileMap = stateMap.get(profileId);
            if (profileMap != null) {
                return new ArrayList<>(profileMap.values());
            }
        }
        return matchNotFound(profileId);
    }

    /**
     * @param profileId     Collection profile ID
     * @return List         list of state
     * @throws ProfileExecutionException    "Exception handled"
     */
    private List<String> matchNotFound(String profileId) throws ProfileExecutionException {
        String msg = String.format("%s profile not found", profileId);
        log.warn(msg);
        throw new ProfileExecutionException(msg);
    }
}