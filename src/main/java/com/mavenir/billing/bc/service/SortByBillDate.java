package com.mavenir.billing.bc.service;

import com.mavenir.billing.bc.model.DueBill;

import java.util.Comparator;

public class SortByBillDate implements Comparator<DueBill> {
    @Override
    public int compare(DueBill bill1, DueBill bill2) {
        return bill1.getBillDate().compareTo(bill2.getBillDate());
    }
}
