package com.mavenir.billing.bc.service;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;

import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.BasicHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.ssl.TrustStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import com.mavenir.billing.bc.config.EmailNotificationConfiguration;
import com.mavenir.billing.bc.model.EmailDTOResponse;
import com.mavenir.billing.bc.model.EmailWithTemplateDTO;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class EmailNotificationService {
    RestTemplate restTemplate;

    @Autowired
    EmailNotificationConfiguration emailNotificationConfiguration;

    /**
     * This method connects to Send Email Notification Service via Restful API call
     * 
     * @param request       "Email With Template DTO"
     * @param emailTemplate "Email Template ID "
     * @return EmailDTOResponse "response"
     * @throws RestClientException         "Exception handling"
     * @throws KeyManagementException      "Exception handling"
     * @throws NoSuchAlgorithmException    "Exception handling"
     * @throws KeyStoreException"Exception handling"
     */
    public EmailDTOResponse sendNotificationEmail(EmailWithTemplateDTO request, String emailTemplate)
            throws RestClientException, KeyManagementException, NoSuchAlgorithmException, KeyStoreException {

        if (restTemplate == null) {
            createRequestFactory();
        }

        String url = emailNotificationConfiguration.getConnectionAddress() + ":" + emailNotificationConfiguration.getPort()
                + emailNotificationConfiguration.getContextPath();
        url = url.replace("{templateId}", emailTemplate);

        try {
            ResponseEntity<EmailDTOResponse> response = restTemplate.postForEntity(url, request, EmailDTOResponse.class);

            if (response.getStatusCode().equals(HttpStatus.OK) || response.getStatusCode().equals(HttpStatus.ACCEPTED)) {
                log.info("Email notification service returned status code: 200 (OK)/202 (ACCEPTED)");
                return response.getBody();
            } else {
                log.error("Email notification service returned status code: " + response.getStatusCode());
            }
        } catch (RestClientException e) {
            log.error("Unable to connect to Email Notification Service.");
            throw e;
        }

        log.error("No Email Notification Response data provided.");
        return null;
    }

    /**
     * This method creates request Factory for RestTemplate (specifically for https
     * connections)
     * 
     * @throws KeyManagementException      "Exception handling"
     * @throws NoSuchAlgorithmException    "Exception handling"
     * @throws KeyStoreException"Exception handling"
     */
    public void createRequestFactory() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException {
        if (emailNotificationConfiguration.getSslFlag()) {
            TrustStrategy acceptingTrustStrategy = (cert, authType) -> true;

            SSLContext sslContext = SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy).build();

            // NoopHostnameVerifier is temporary until certificate is provided
            SSLConnectionSocketFactory sslsf = new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE);

            Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory>create()
                    .register("https", sslsf).register("http", new PlainConnectionSocketFactory()).build();

            BasicHttpClientConnectionManager connectionManager = new BasicHttpClientConnectionManager(socketFactoryRegistry);

            CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(sslsf)
                    .setConnectionManager(connectionManager).build();

            HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(httpClient);

            restTemplate = new RestTemplate(requestFactory);
        } else {
            restTemplate = new RestTemplate();
        }
    }
}
