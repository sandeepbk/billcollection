package com.mavenir.billing.bc.service;

import com.mavenir.billing.bc.config.DunningConfiguration;
import com.mavenir.billing.bc.constants.InvoiceStatus;
import com.mavenir.billing.bc.exception.Error;
import com.mavenir.billing.bc.exception.ProcessException;
import com.mavenir.billing.bc.exception.ServiceException;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.InvoiceResponse;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.billing.bc.util.CollectionUtils;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
@NoArgsConstructor
public class InvoiceService {

    public static final String GET_ACCOUNT_INFO = "get-document-info";
    public static final String GET_HISTORY_INVOICE_INFO = "get-history-invoice-info";
    public static final String GET_DUE_INVOICE_INFO = "get-due-invoice-info";
    public static final String INVALID_DATE_RANGE = "invalid date range";

    @Autowired
    BillingRepository billingRepository;

    @Autowired
    private DunningConfiguration dunningConfiguration;

    @Autowired
    private CollectionUtils collectionUtils;

    /**
     * @param accountId
     * @return HistoryBillDTO
     * @throws ProcessException
     */
    public List<InvoiceResponse> getHistoryInvoicesByAccountId(String accountId) {

        log.info("Fetching History/Due Invoices for AccountId {}", accountId);

        List<InvoiceResponse> invoices = new ArrayList<>();

        final String documentId = dunningConfiguration.getDocumentID().concat(accountId);

        log.info("Document Id {}", documentId);

        Optional<Billing> billingDetailOptional = billingRepository
                .findById(documentId);

        if (billingDetailOptional.isEmpty()) {
            log.error("No document present for the given {} documentId", documentId);
            throw new ServiceException(GET_ACCOUNT_INFO, Error.DOCUMENT_NOT_FOUND);
        }

        ImmutablePair<Date, Date> dates = collectionUtils.getDates();

        Date fromDate = dates.left;
        Date toDate = dates.right;

        log.info("from date {} toDate {} ", fromDate, toDate);

        try {
            billingDetailOptional.get()
                    .getHistoryBill()
                    .stream()
                    .forEach(historyBill -> {
                        if (collectionUtils.checkDateRange(fromDate, toDate, historyBill.getBillDate())) {
                        InvoiceResponse invoiceResponse = InvoiceResponse.builder()
                                .invoiceNumber(historyBill.getInvoiceId())
                                .balance(Double.valueOf(historyBill.getOutstandingAmount()))
                                .date(collectionUtils.dateToString(historyBill.getBillDate()))
                                .status(InvoiceStatus.Paid.name())
                                .amountPaid(collectionUtils.getPaidAmount(Double.valueOf(historyBill.getDueAmount()), Double.valueOf(historyBill.getOutstandingAmount())))
                                .build();
                        invoices.add(invoiceResponse);
                    } });
            Collections.sort(invoices);
        } catch (Exception exception) {
            log.error("Error Occurs while fetching history invoices for given {} documentId", documentId);
            throw new ServiceException(GET_HISTORY_INVOICE_INFO, Error.FETCH_HISTORY_INVOICE_FAILED);
        }

        try {
            billingDetailOptional.get()
                    .getDueBill()
                    .stream()
                    .forEach(dueBill -> {
                        if (collectionUtils.checkDateRange(fromDate, toDate, dueBill.getBillDate())) {
                        InvoiceResponse invoiceResponse = InvoiceResponse.builder()
                                .invoiceNumber(dueBill.getInvoiceId())
                                .balance(Double.valueOf(dueBill.getOutstandingAmount()))
                                .date(collectionUtils.dateToString(dueBill.getBillDate()))
                                .status(collectionUtils.getInvoiceStatusForDueBill(Double.valueOf(dueBill.getOutstandingAmount()),
                                        Double.valueOf(dueBill.getDueAmount())))
                                .amountPaid(collectionUtils.getPaidAmount(Double.valueOf(dueBill.getDueAmount()), Double.valueOf(dueBill.getOutstandingAmount())))
                                .build();
                        invoices.add(invoiceResponse);
                    } });
            Collections.sort(invoices);
        } catch (Exception exception) {
            log.error("Exception Occurs while fetching Due invoice details", exception);
            throw new ServiceException(GET_DUE_INVOICE_INFO, Error.FETCH_DUE_INVOICE_FAILED);
        }
        return invoices;
    }

    public List<InvoiceResponse> getHistoryInvoicesByDates(String accountId, String fromBillDate, String toBillDate) {

        log.info("Fetching History/Due Invoices for AccountId {}, FromBillDate {}, ToBillDate {}", accountId, fromBillDate, toBillDate);

        if (!collectionUtils.validateDateRange(fromBillDate, toBillDate)) {
            throw new ServiceException(INVALID_DATE_RANGE, Error.DATE_RANGE_INVALID);
        }
        List<InvoiceResponse> invoices = new ArrayList<>();

        final String documentId = dunningConfiguration.getDocumentID().concat(accountId);
        log.info("Document Id {}", documentId);

        Optional<Billing> billingDetailOptional = billingRepository
                .findById(documentId);

        if (billingDetailOptional.isEmpty()) {
            log.error("No document present for the given {} documentId", documentId);
            throw new ServiceException(GET_ACCOUNT_INFO, Error.DOCUMENT_NOT_FOUND);
        }

        Date fromDate = collectionUtils.stringToDate(fromBillDate);
        Date toDate = collectionUtils.stringToDate(toBillDate);

        try {
            billingDetailOptional.get()
                    .getHistoryBill()
                    .stream()
                    .forEach(historyBill -> {
                        if (collectionUtils.checkDateRange(fromDate, toDate, historyBill.getBillDate())) {
                            InvoiceResponse invoiceResponse = InvoiceResponse.builder()
                                    .invoiceNumber(historyBill.getInvoiceId())
                                    .balance(Double.valueOf(historyBill.getOutstandingAmount()))
                                    .date(collectionUtils.dateToString(historyBill.getBillDate()))
                                    .status(InvoiceStatus.Paid.name())
                                    .amountPaid(collectionUtils.getPaidAmount(Double.valueOf(historyBill.getDueAmount()), Double.valueOf(historyBill.getOutstandingAmount())))
                                    .build();
                            invoices.add(invoiceResponse);
                        }
                    });
            Collections.sort(invoices);
        } catch (Exception exception) {
            log.error("Error Occurs while fetching history invoices for given {} documentId", documentId);
            throw new ServiceException(GET_HISTORY_INVOICE_INFO, Error.FETCH_HISTORY_INVOICE_FAILED);
        }

        try {
            billingDetailOptional.get()
                    .getDueBill()
                    .stream()
                    .forEach(dueBill -> {
                        if (collectionUtils.checkDateRange(fromDate, toDate, dueBill.getBillDate())) {
                            InvoiceResponse invoiceResponse = InvoiceResponse.builder()
                                    .invoiceNumber(dueBill.getInvoiceId())
                                    .balance(Double.valueOf(dueBill.getOutstandingAmount()))
                                    .date(collectionUtils.dateToString(dueBill.getBillDate()))
                                    .status(collectionUtils.getInvoiceStatusForDueBill(Double.valueOf(dueBill.getOutstandingAmount()),
                                            Double.valueOf(dueBill.getDueAmount())))
                                    .amountPaid(collectionUtils.getPaidAmount(Double.valueOf(dueBill.getDueAmount()), Double.valueOf(dueBill.getOutstandingAmount())))
                                    .build();
                            invoices.add(invoiceResponse);
                        }
                    });
            Collections.sort(invoices);
        } catch (Exception exception) {
            log.error("Exception Occurs while fetching Due invoice details", exception);
            throw new ServiceException(GET_DUE_INVOICE_INFO, Error.FETCH_DUE_INVOICE_FAILED);
        }
        return invoices;
    }
}
