package com.mavenir.billing.bc.service;

import com.mavenir.billing.bc.config.BillCollectionDunningConfiguration;
import com.mavenir.billing.bc.config.DunningConfiguration;
import com.mavenir.billing.bc.constants.BillCollectionConstants;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.CollectionDocument;
import com.mavenir.billing.bc.model.DueBill;
import com.mavenir.billing.bc.repository.BillingRepository;
import com.mavenir.dep.common.profileexecutor.ProfileExecutorLibrary;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@AllArgsConstructor
public class BillCollectionService {

    @Autowired
    private BillCollectionDunningConfiguration billCollectionDunningConfiguration;

    @Autowired
    private BillingRepository billingRepository;

    @Autowired
    private BillDunningService billDunningService;

    @Autowired
    private DunningConfiguration dunningConfiguration;

    /**
     * @param collectionDocument "collection document object"
     * @return boolean              "boolean "
     * @throws Exception "Exception handling"
     */
    public boolean paymentDueDateExceeded(CollectionDocument collectionDocument) throws Exception {
        log.info("Billing account ID {}", collectionDocument.getBillingAccId());
        ProfileExecutorLibrary<String, String, Billing, DueBill> profileExecutorLibrary =
                billCollectionDunningConfiguration.getProfileExecutorLibrary();
        Billing billing = billDunningService.getBillingIdFromDB(collectionDocument.getBillingAccId());
        List<DueBill> updatedDueBillList = new ArrayList<>();
        for (DueBill dueBill : billing.getDueBill()) {
            log.info("Dunning invoice ID {}", dueBill.getInvoiceId());
            if(dueBill.getInvoiceId().equals(collectionDocument.getInvoiceId())) {
                String nextState = profileExecutorLibrary.processEvent(billing.getDunningProfileId(),
                        dueBill.getBillState().toUpperCase(), collectionDocument.getEvent().toUpperCase(), billing, dueBill);
                log.info("State changed from {} to {}", dueBill.getBillState(), nextState);
                billDunningService.processDunningEvent(billing, dueBill, collectionDocument.getEvent());
                if (nextState.equals(BillCollectionConstants.SUSPENDED) ||
                        nextState.equals(BillCollectionConstants.TERMINATE)) {
                    billing.setAccountState(nextState);
                    dueBill.setServiceState(nextState);
                }
                dueBill.setBillState(nextState.toUpperCase());
            }
            updatedDueBillList.add(dueBill);
        }
        billing.setDueBill(updatedDueBillList);
        billingRepository.save(billing, dunningConfiguration.getDocumentID().concat(billing.getBillingAccId()));
        return true;
    }
}