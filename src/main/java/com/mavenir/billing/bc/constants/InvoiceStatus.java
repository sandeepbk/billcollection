package com.mavenir.billing.bc.constants;

public enum InvoiceStatus {
    Paid,
    PartiallyPaid,
    Pending,
    Unpaid
}
