package com.mavenir.billing.bc.constants;

public class BillCollectionConstants {
    public static final String JSON ="json";
    public static final String YAML ="yaml";

    public static final String PENALTY_AMOUNT ="PENALTY_AMOUNT";
    public static final String EMAIL_TEMPLATE ="EMAILTEMPLATE";
    public static final String NEXT_EVENT ="nextEvent";

    public static final String SEND_EMAIL ="SENDEMAIL";
    public static final String SCHEDULE ="SCHEDULE";
    public static final String SUSPENDED ="SUSPENDED";
    public static final String PENALTY ="PENALTY";
    public static final String TERMINATE ="TERMINATE";
    public static final String RESUME ="RESUME";

    public static final String MONTH_OFFSET = "monthoffset";
    public static final String PLUS_DAYS = "plusdays";
    public static final String MINUS_DAYS = "minusdays";

    public static final String DATE_FORMAT_YYYYMMDD = "yyyyMMdd";
    public static final String DATE_FORMAT_DDMMYYYY = "ddMMyyyy";
    public static final String DATE_FORMAT_YEAR = "yyyy";
    public static final String DATE_FORMAT_MONTH = "MM";
    public static final String DATE_FORMAT_DAY = "dd";

    public static final String FULL_PAYMENT ="FULL_PAYMENT";
    public static final int OK_200 = 200;
    public static final String SUCCESS = "SUCCESS";
    public static final String NA ="NA";
    public static final String EMPTY ="";
    public static final String FAILED ="FAILED";

    public static final String ACTIVE = "ACTIVE";
    public static final String DUNNING_TERMINATED = "Terminated";
    public static final String DUNNING_SUSPENDED = "Suspended";

    public static final String EVENT0 = "EVENT0";
    public static final String PRIMARY_EMAIL_TYPE = "PRIMARY";

}
