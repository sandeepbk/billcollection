package com.mavenir.billing.bc.repository;

import com.couchbase.client.core.error.CasMismatchException;
import com.couchbase.client.core.error.DecodingFailureException;
import com.couchbase.client.core.error.DocumentExistsException;
import com.couchbase.client.core.error.DocumentNotFoundException;
import com.couchbase.client.java.Bucket;
import com.couchbase.client.java.Cluster;
import com.couchbase.client.java.Collection;
import com.couchbase.client.java.kv.GetResult;
import com.couchbase.client.java.kv.MutationResult;
import com.couchbase.client.java.kv.ReplaceOptions;
import com.couchbase.client.java.query.QueryResult;
import com.mavenir.billing.bc.model.Billing;
import com.mavenir.billing.bc.model.EmailBillingInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Slf4j
@Component
public class BillingRepository {
    private final Cluster cluster;
    private final Bucket bucket;
    private final Collection collection;

    @Autowired
    public BillingRepository(Cluster cluster, Bucket bucket) {
        this.cluster = cluster;
        this.bucket = bucket;
        this.collection = bucket.defaultCollection();
    }

    public Optional<Billing> findById(String documentId) {
        log.debug("Entered findById. Input param: {}", documentId);
        Optional<Billing> opt = Optional.empty();
        try {
            GetResult result = collection.get(documentId);
            Billing billing = result.contentAs(Billing.class);
            billing.setVersion(String.valueOf(result.cas()));
            opt = Optional.ofNullable(billing);
        } catch (DocumentNotFoundException | DecodingFailureException e) {
        }
        log.debug("Exiting findById()");
        return opt;
    }

    public Billing save(Billing billing, String documentId) {
        MutationResult result;
        String version = billing.getVersion();
        billing.setVersion(null);
        if (StringUtils.isNotBlank(version)) {
            log.debug("Replace document {}", documentId);
            long cas = Long.parseLong(version);
            ReplaceOptions options = ReplaceOptions.replaceOptions().cas(cas);
            try {
                result = collection.replace(documentId, billing, options);
            } catch (CasMismatchException e) {
                throw e;
            } catch (DocumentNotFoundException e) {
                throw e;
            }
        } else {
            log.debug("Insert document {}", documentId);
            try {
                result = collection.insert(documentId, billing);
            } catch (DocumentExistsException e) {
                throw e;
            }
        }
        billing.setVersion(String.valueOf(result.cas()));
        return billing;
    }

    public List<EmailBillingInfo> getEmailBillingInfo(String accountId) {
        String query = "SELECT "+this.bucket.name()+".* FROM "+this.bucket.name()+" where accountId = '"+accountId+"'";
        log.debug("Query: \"{}\", Parameters: {}", query, accountId);
        QueryResult result = cluster.query(query);
        return result.rowsAs(EmailBillingInfo.class);
    }
}