FROM openjdk:11.0.4-jre
RUN apt-get update \
    && apt-get install -y dnsutils \
    && apt-get install -y procps \
    && apt-get install -y vim
VOLUME [ "/tmp" ]
ARG DEPENDENCY=target/dependency
COPY ${DEPENDENCY}/BOOT-INF/lib /app/lib
COPY ${DEPENDENCY}/META-INF /app/META-INF
COPY ${DEPENDENCY}/BOOT-INF/classes /app
ENV CLASSPATH=$CLASSPATH:/app/lib/*:/app
# /tmp => spring-boot creates tomcat working directories
# To reduce tomcat startup time system property /dev/urandom as source of entropy
ENTRYPOINT [ "java", "-Djava.security.egd=file:/dev/./urandom", "-Dspring.profiles.active=${SPRING_PROFILES_ACTIVE}", "com.mavenir.billing.bc.BillCollectionApplication"]